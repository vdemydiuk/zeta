#ifndef SIP_ENDPOINT_H
#define SIP_ENDPOINT_H

#include <atomic>
#include <vector>
#include <pjlib.h>
#include <pjmedia.h>
#include <pjsip.h>
#include <pjsip_ua.h>
#include <pjsua.h>
#include "log/logger.h"
#include "config.h"
#include <boost/signals2.hpp>

namespace zt
{
    class Call;
    struct SessionProvider;

    namespace sip
    {
        class CallSlotCollection;
        
        static const char* MODE_NAME = "mod-siprtpapp";
        static constexpr uint32_t MAX_CALLS_NUM  = 1024;

        class SipEndpoint
        {
            typedef boost::signals2::signal<void (const std::shared_ptr<Call>& call)> OnIncomingCallSignal;

        public:
            SipEndpoint(SessionProvider& provider);
            SipEndpoint(SipEndpoint const&)        = delete;
            void operator=(SipEndpoint const&)     = delete;
            
            ~SipEndpoint();
            
            bool Create(const Config& config);
            void Destroy();

            bool isCreated() { return is_created_; }
            
            std::shared_ptr<Call> MakeCall(const std::string& dst_uri);
            
            std::string getLocalAddress() const { return local_addr_; }
            int getLocalPort() const { return local_port_; }
            std::string getTransportType() const { return transport_type_; }

            OnIncomingCallSignal OnIncomingCallEvent;
            
        private:
            static void LogWriter(int level, const char *buffer, int len);
            
            /* Callback to be called when invite session's state has changed: */
            static void CallOnStateChanged( pjsip_inv_session *inv,
                                           pjsip_event *e);
            
            /* Callback to be called when dialog has forked: */
            static void CallOnForked(pjsip_inv_session *inv,
                                     pjsip_event *e);
            
            /* Callback to be called when SDP negotiation is done in the call: */
            static void CallOnMediaUpdate( pjsip_inv_session *inv,
                                          pj_status_t status);
            
            /* Callback to be called to handle incoming requests outside dialogs: */
            static pj_bool_t OnRxRequest(pjsip_rx_data *rdata);
            
            /* Callback to be called to process incoming response message. */
            static pj_bool_t OnRxResponse(pjsip_rx_data *rdata);
            
            /* Worker thread for SIP */
            static int SipWorkerThread(void *arg);
            
            void ProcessCallState(pjsip_inv_session *inv);
            
            /* Receive incoming call */
            void ProcessIncomingCall(pjsip_rx_data *rdata);
            
            /* Receive media negotiated */
            void ProcessMediaUpdate(pjsip_inv_session *inv);

            void ReleasePjLib();
            pj_status_t InitSipTransport(bool udp, pjsua_transport_config& config, int af);
            std::string CreateUasContact() const;
            bool VerifySipUrl(const std::string& sip_url);
            
            static log::Logger      log_;

            bool                    is_created_{false};
            
            pj_caching_pool         cp_;
            pj_pool_t*              pool_;
            
            pjsip_endpoint*         sip_endpt_{nullptr};
            int                     local_port_{-1};
            std::string             local_addr_;
            std::string             transport_type_;
            
            std::atomic<bool>           is_threads_run_{false};
            std::vector<pj_thread_t*>   threads_;
            
            std::unique_ptr<CallSlotCollection>     call_slots_;

            static SipEndpoint*     instance_;
            SessionProvider&        session_provider_;
            
            pjsip_module            mod_siprtp_ =
            {
                NULL, NULL,                     /* prev, next.              */
                { (char*)MODE_NAME, 13 },       /* Name.                    */
                -1,                             /* Id                       */
                PJSIP_MOD_PRIORITY_APPLICATION, /* Priority                 */
                NULL,                           /* load()                   */
                NULL,                           /* start()                  */
                NULL,                           /* stop()                   */
                NULL,                           /* unload()                 */
                &OnRxRequest,                   /* on_rx_request()          */
                &OnRxResponse,                  /* on_rx_response()         */
                NULL,                           /* on_tx_request.           */
                NULL,                           /* on_tx_response()         */
                NULL,                           /* on_tsx_state()           */
            };
        };
    }
}

#endif //SIP_ENDPOINT_H
