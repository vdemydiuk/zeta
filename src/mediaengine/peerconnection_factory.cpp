//
//  peerconnection_factory.cpp
//  PeerConnectionFactory
//
//  Created by Vyacheslav Demidyuk on 11/18/21.
//

#include "mediaengine/peerconnection_factory.h"
#include "api/audio_codecs/audio_decoder_factory.h"
#include "api/audio_codecs/audio_encoder_factory.h"
#include "api/audio_codecs/builtin_audio_decoder_factory.h"
#include "api/audio_codecs/builtin_audio_encoder_factory.h"
#include "api/create_peerconnection_factory.h"
#include "api/video_codecs/video_decoder_factory.h"
#include "api/video_codecs/video_encoder_factory.h"
#include "api/call/call_factory_interface.h"
#include "api/peer_connection_interface.h"
#include "api/rtc_event_log/rtc_event_log_factory.h"
#include "api/task_queue/default_task_queue_factory.h"
#include "media/base/media_engine.h"
#include "media/engine/webrtc_media_engine.h"
#include "p2p/client/basic_port_allocator.h"
#include "rtc_base/thread.h"

#include "modules/video_coding/codecs/h264/include/h264.h"

#include "modules/video_coding/codecs/av1/libaom_av1_encoder.h"
#include "modules/video_coding/codecs/av1/libaom_av1_decoder.h"

//#include "api/audio_codecs/audio_encoder_factory.h"
//#include "api/audio_codecs/audio_encoder_factory_template.h"
//#include "api/audio_codecs/audio_decoder_factory_template.h"
//#include "api/audio_codecs/g711/audio_encoder_g711.h"
//#include "api/audio_codecs/g711/audio_decoder_g711.h"
//#include "mediaengine/audio_decoder_factory.h"

#include <boost/algorithm/string.hpp>

using namespace zt;
using namespace webrtc;

const char kAudioLabel[] = "audio_label";
const char kVideoLabel[] = "video_label";
const char kStreamId[] = "stream_id";

class VideoTrackSourceStub : public webrtc::VideoTrackSourceInterface
{
public:
    VideoTrackSourceStub() = default;

    // VideoTrackSourceInterface
    bool is_screencast() const override { return false; }
    absl::optional<bool> needs_denoising() const override { return false; }
    bool GetStats(VideoTrackSourceInterface::Stats*) override { return false; }

    // MediaSourceInterface
    MediaSourceInterface::SourceState state() const override { return MediaSourceInterface::SourceState::kLive; }
    bool remote() const override { return false; }

    // NotifierInterface
    void RegisterObserver(ObserverInterface* observer) override {}
    void UnregisterObserver(ObserverInterface* observer) override {}

    // rtc::VideoSourceInterface<VideoFrame>
    void AddOrUpdateSink(rtc::VideoSinkInterface<VideoFrame>*, const rtc::VideoSinkWants&) override {}
    void RemoveSink(rtc::VideoSinkInterface<VideoFrame>*) override {}
    bool SupportsEncodedOutput() const override { return false; }
    void GenerateKeyFrame() override {}
    void AddEncodedSink(rtc::VideoSinkInterface<RecordableEncodedFrame>*) override {}
    void RemoveEncodedSink(rtc::VideoSinkInterface<RecordableEncodedFrame>*) override {}
};

class CustomEncoderFactory : public VideoEncoderFactory 
{
public:
    static std::vector<SdpVideoFormat> SupportedFormats();
    std::vector<SdpVideoFormat> GetSupportedFormats() const override;
    CodecSupport QueryCodecSupport(const SdpVideoFormat& format,
        absl::optional<std::string> scalability_mode) const override;
    std::unique_ptr<VideoEncoder> CreateVideoEncoder(
        const SdpVideoFormat& format) override;
};

class CustomDecoderFactory : public VideoDecoderFactory
{
public:
    std::vector<SdpVideoFormat> GetSupportedFormats() const override;
    CodecSupport QueryCodecSupport(const SdpVideoFormat& format,
                                    bool reference_scaling) const override;
    std::unique_ptr<VideoDecoder> CreateVideoDecoder(
        const SdpVideoFormat& format) override;
};

std::vector<SdpVideoFormat> CustomEncoderFactory::SupportedFormats()
{
    std::vector<SdpVideoFormat> supported_codecs;
    // for (const SdpVideoFormat& format : SupportedH264Codecs())
    //     supported_codecs.push_back(format);
    supported_codecs.push_back(SdpVideoFormat(cricket::kAv1CodecName));
    return supported_codecs;
}

std::vector<SdpVideoFormat> CustomEncoderFactory::GetSupportedFormats() const 
{
  return SupportedFormats();
}

std::unique_ptr<VideoEncoder> CustomEncoderFactory::CreateVideoEncoder(
    const SdpVideoFormat& format) 
{
    // if (boost::iequals(format.name, std::string(cricket::kH264CodecName)))
    //     return H264Encoder::Create(cricket::VideoCodec(format));
    if (kIsLibaomAv1DecoderSupported &&
        boost::iequals(format.name, std::string(cricket::kAv1CodecName)))
        return CreateLibaomAv1Encoder();
    return nullptr;
}

VideoEncoderFactory::CodecSupport CustomEncoderFactory::QueryCodecSupport(
    const SdpVideoFormat& format, absl::optional<std::string> scalability_mode) const 
{    
    CodecSupport codec_support;
    codec_support.is_supported = format.IsCodecInList(GetSupportedFormats());
    return codec_support;
}

std::vector<SdpVideoFormat> CustomDecoderFactory::GetSupportedFormats() const 
{
    std::vector<SdpVideoFormat> formats;
    // for (const SdpVideoFormat& h264_format : SupportedH264Codecs())
    //     formats.push_back(h264_format);
    if (kIsLibaomAv1DecoderSupported)
        formats.push_back(SdpVideoFormat(cricket::kAv1CodecName));
    return formats;
}

VideoDecoderFactory::CodecSupport CustomDecoderFactory::QueryCodecSupport(
    const SdpVideoFormat& format, bool reference_scaling) const 
{
    CodecSupport codec_support;
    codec_support.is_supported = format.IsCodecInList(GetSupportedFormats());
    return codec_support;
}

std::unique_ptr<VideoDecoder> CustomDecoderFactory::CreateVideoDecoder(
    const SdpVideoFormat& format) 
{
    if (!format.IsCodecInList(GetSupportedFormats())) 
    {
        // RTC_LOG(LS_WARNING) << "Trying to create decoder for unsupported format. "
        //                     << format.ToString();
        return nullptr;
    }

    // if (boost::iequals(format.name, std::string(cricket::kH264CodecName)))
    //     return H264Decoder::Create();
    if (kIsLibaomAv1DecoderSupported &&
        boost::iequals(format.name, std::string(cricket::kAv1CodecName)))
        return CreateLibaomAv1Decoder();
    return nullptr;
}

PeerConnectionFactory::PeerConnectionFactory()
:log_("PeerConnectionFactory")
{
    network_thread_ = rtc::Thread::CreateWithSocketServer();
    worker_thread_ = rtc::Thread::CreateWithSocketServer();
    signaling_thread_ = rtc::Thread::CreateWithSocketServer();

    network_thread_->Start();
    worker_thread_->Start();
    signaling_thread_->Start();

    PeerConnectionFactoryDependencies pcf_deps;
    pcf_deps.network_thread = network_thread_.get();
    pcf_deps.worker_thread = worker_thread_.get();
    pcf_deps.signaling_thread = signaling_thread_.get();
    pcf_deps.task_queue_factory = CreateDefaultTaskQueueFactory();
    pcf_deps.call_factory = CreateCallFactory();
    pcf_deps.event_log_factory = std::make_unique<RtcEventLogFactory>(
        pcf_deps.task_queue_factory.get());

    cricket::MediaEngineDependencies media_deps;
    media_deps.task_queue_factory = pcf_deps.task_queue_factory.get();
    media_deps.audio_encoder_factory = /*CreateAudioEncoderFactory<AudioEncoderG711>();*/ webrtc::CreateBuiltinAudioEncoderFactory();
    media_deps.audio_decoder_factory = /*AudioCodecHelper::CreateCustomAudioDecoderFactory();*/ webrtc::CreateBuiltinAudioDecoderFactory();
    media_deps.audio_processing = AudioProcessingBuilder().Create();
    media_deps.video_encoder_factory = std::make_unique<CustomEncoderFactory>();
    media_deps.video_decoder_factory = std::make_unique<CustomDecoderFactory>();

    pcf_deps.media_engine = cricket::CreateMediaEngine(std::move(media_deps));

    peer_connection_factory_ = CreateModularPeerConnectionFactory(std::move(pcf_deps));

    PeerConnectionFactoryInterface::Options options;
    options.disable_encryption = true;
    peer_connection_factory_->SetOptions(options);
}

PeerConnectionFactory::~PeerConnectionFactory()
{
    peer_connection_factory_ = nullptr;

    network_thread_->Stop();
    worker_thread_->Stop();
    signaling_thread_->Stop();
}

rtc::scoped_refptr<PeerConnectionInterface> PeerConnectionFactory::CreatePeerConnection(PeerConnectionObserver* observer)
{
    log_.Trace("%s", __FUNCTION__);

    PeerConnectionInterface::RTCConfiguration config;
    config.sdp_semantics = SdpSemantics::kUnifiedPlan;
    config.tcp_candidate_policy = PeerConnectionInterface::kTcpCandidatePolicyDisabled;
    config.disable_ipv6 = true;
    config.bundle_policy = PeerConnectionInterface::kBundlePolicyBalanced;

    auto peer_connection = peer_connection_factory_->CreatePeerConnection(
        config, nullptr, nullptr, observer);

    if (!peer_connection)
    {
        log_.Error("%s: create peer connection failed!", __FUNCTION__);
        return nullptr;
    }

    // add audio stream
    rtc::scoped_refptr<AudioTrackInterface> audio_track(
        peer_connection_factory_->CreateAudioTrack(
            kAudioLabel, peer_connection_factory_->CreateAudioSource(
                            cricket::AudioOptions())));
    auto result_or_error = peer_connection->AddTrack(audio_track, {kStreamId});
    if (!result_or_error.ok()) 
    {
        log_.Error("%s: Failed to add audio track to PeerConnection: %s", result_or_error.error().message());
        return nullptr;
    }

    // add video stream
    rtc::scoped_refptr<VideoTrackInterface> video_track(
        peer_connection_factory_->CreateVideoTrack(
                kVideoLabel, new rtc::RefCountedObject<VideoTrackSourceStub>()));
    result_or_error = peer_connection->AddTrack(video_track, {kStreamId});
    if (!result_or_error.ok()) 
    {
        log_.Error("%s: Failed to add video track to PeerConnection: %s", result_or_error.error().message());
        return nullptr;
    }

    return peer_connection;
}