#ifndef LOG_LOGLEVEL_H
#define LOG_LOGLEVEL_H

namespace zt
{
    namespace log
    {
        enum class LogLevel
        {
            FATAL,
            ERROR,
            WARNING,
            INFO,
            DEBUG,
            TRACE
        };
    } //log
} //zt
#endif //LOG_LOGLEVEL_H
