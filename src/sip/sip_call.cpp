#include "sip/sip_call.h"
#include "sdp/sdp_parser.h"
#include "sdp/session_description.h"
#include "common/session.h"
#include <pjlib.h>
#include <pjmedia.h>
#include <pjsip.h>
#include <mutex>
#include <condition_variable>

using namespace zt;
using namespace zt::sip;

SipCall::SipCall(pjsip_inv_session* inv, std::unique_ptr<Session> session)
:log_("SipCall")
,inv_(inv)
,session_(std::move(session))
{
    /* Mark start of call */
    pj_gettimeofday(&start_time_);
}

SipCall::~SipCall()
{
}

bool SipCall::isActive()
{
    std::lock_guard<std::recursive_mutex> lock(mutex_);
    return inv_ != NULL && inv_->state != PJSIP_INV_STATE_DISCONNECTED;
}

CallState SipCall::getCallState()
{
    std::lock_guard<std::recursive_mutex> lock(mutex_);
    return toCallState(inv_->state);
}

bool SipCall::Invite()
{
    log_.Trace("%s: entry.", __FUNCTION__);
    
    auto sdp = CreateSessionDescription();
    if (sdp == nullptr)
    {
        log_.Error("%s: Session description is not created!", __FUNCTION__);
        return false;
    }
    auto sdp_str = sdp->toString();
    
    std::lock_guard<std::recursive_mutex> lock(mutex_);
    
    /* Convert sdp to pjmedia_sdp_session */
    pjmedia_sdp_session* pjsip_sdp;
    auto status = pjmedia_sdp_parse(inv_->dlg->pool, (char*)sdp_str.c_str(), sdp_str.size(), &pjsip_sdp);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Failed to parse sdp offer.", __FUNCTION__);
        pjsip_dlg_terminate(inv_->dlg);
        return false;
    }

    char buffer[8000];
    auto size = pjmedia_sdp_print(pjsip_sdp, buffer, sizeof(buffer));
    if (size > 0)
    {
        buffer[size] = '\0';
        log_.Debug("%s: SDP [%d]: \n%s", __FUNCTION__, sdp_str.size(), buffer);
    }
    
    status = pjmedia_sdp_validate(pjsip_sdp);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Failed sdp validation. [%d]", __FUNCTION__, status);
        return false;
    }
    
    status = pjsip_inv_set_local_sdp(inv_, pjsip_sdp);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Failed to apply offer sdp to invite session.", __FUNCTION__);
        return false;
    }
    
    pjmedia_sdp_neg_set_answer_multiple_codecs(inv_->neg, PJ_TRUE);
    
    /* Create initial INVITE request. */
    pjsip_tx_data *tdata;
    status = pjsip_inv_invite(inv_, &tdata);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Failed to create initial INVITE request", __FUNCTION__);
        return false;
    }
    
    /* Send initial INVITE request.
     * From now on, the invite session's state will be reported to us
     * via the invite session callbacks.
     */
    status = pjsip_inv_send_msg(inv_, tdata);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Failed to send initial INVITE request", __FUNCTION__);
        return false;
    }
    
    log_.Debug("%s: sent invite with sdp \n%s", __FUNCTION__, sdp_str.c_str());
    
    return true;
}

bool SipCall::Answer()
{
    log_.Trace("%s: entry.", __FUNCTION__);

    auto sdp = CreateSessionDescription();
    if (sdp == nullptr)
    {
        log_.Error("%s: Session description is not created!", __FUNCTION__);
        return false;
    }
    auto sdp_str = sdp->toString();
    
    log_.Debug("%s: prepared sdp answer\n%s", __FUNCTION__, sdp_str.c_str());
    
    std::lock_guard<std::recursive_mutex> lock(mutex_);
    
    /* Convert sdp to pjmedia_sdp_session */
    pjmedia_sdp_session* pjsip_sdp;
    auto status = pjmedia_sdp_parse(inv_->dlg->pool, (char*)sdp_str.c_str(), sdp_str.size(), &pjsip_sdp);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Failed to parse sdp answer. status = %d", __FUNCTION__, status);
        return false;
    }
    
    status = pjmedia_sdp_validate(pjsip_sdp);
    if (status != PJ_SUCCESS) {
        log_.Error("%s: Failed sdp validation.", __FUNCTION__);
        return false;
    }
    
    status = pjsip_inv_set_local_sdp(inv_, pjsip_sdp);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Failed to apply answer sdp to invite session.", __FUNCTION__);
        return false;
    }
    
    pjmedia_sdp_neg_set_answer_multiple_codecs(inv_->neg, PJ_TRUE);
    
    /* Create 200 response .*/
    pjsip_tx_data *tdata;
    status = pjsip_inv_answer(inv_, 200,
                              NULL, NULL, &tdata);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Failed to create 200 response. status = %d", __FUNCTION__, status);
        
        status = pjsip_inv_answer(inv_,
                                  PJSIP_SC_NOT_ACCEPTABLE,
                                  NULL, NULL, &tdata);
        if (status == PJ_SUCCESS)
            pjsip_inv_send_msg(inv_, tdata);
        else
            pjsip_inv_terminate(inv_, 500, PJ_FALSE);
        
        return false;
    }
    
    /* Send the 200 response. */
    status = pjsip_inv_send_msg(inv_, tdata);
    
    return true;
}

bool SipCall::Hangup(const std::string& reason /*= ""*/)
{
    log_.Trace("%s: entry.", __FUNCTION__);
    
    std::lock_guard<std::recursive_mutex> lock(mutex_);
    
    unsigned code;
    if (inv_->state == PJSIP_INV_STATE_CONFIRMED)
        code = PJSIP_SC_OK;
    else if (inv_->role == PJSIP_ROLE_UAS)
        code = PJSIP_SC_DECLINE;
    else
        code = PJSIP_SC_REQUEST_TERMINATED;
    
    pj_str_t pjstr_reason;
    pj_cstr(&pjstr_reason, reason.c_str());
    
    pjsip_tx_data *tdata;
    auto status = pjsip_inv_end_session(inv_, code, &pjstr_reason, &tdata);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Failed to create end session message", __FUNCTION__);
        return false;
    }
    
    /* pjsip_inv_end_session may return PJ_SUCCESS with NULL
     * as p_tdata when INVITE transaction has not been answered
     * with any provisional responses.
     */
    if (tdata)
    {
        /* Send the message */
        status = pjsip_inv_send_msg(inv_, tdata);
        if (status != PJ_SUCCESS)
        {
            log_.Error("%s: Failed to send end session message", __FUNCTION__);
            return false;
        }
    }
    
    return true;
}

void SipCall::OnNegotiationCompleted(const pjmedia_sdp_session* local_sdp, const pjmedia_sdp_session* remote_sdp)
{
    log_.Trace("%s: entry.", __FUNCTION__);
    
    std::lock_guard<std::recursive_mutex> lock(mutex_);
    
    /* Convert pjmedia sdp session to string */
    char buffer[2000];
    
    auto size = pjmedia_sdp_print(local_sdp, buffer, sizeof(buffer));
    if (size <= 0)
    {
        log_.Warning("%s: Failed to convert local sdp.", __FUNCTION__);
        return;
    }
    buffer[size] = '\0';
    
    sdp::SessionDescription local_active_sdp;
    sdp::SdpParser::Parse(std::string(buffer), local_active_sdp);
    log_.Trace("%s: negotiated active local sdp:\n%s", __FUNCTION__, local_active_sdp.toString().c_str());
    
    size = pjmedia_sdp_print(remote_sdp, buffer, sizeof(buffer));
    if (size <= 0)
    {
        log_.Warning("%s: Failed to convert remote sdp.", __FUNCTION__);
        return;
    }
    buffer[size] = '\0';
    
    sdp::SessionDescription remote_active_sdp;
    sdp::SdpParser::Parse(std::string(buffer), remote_active_sdp);
    log_.Trace("%s: negotiated active remote sdp:\n%s", __FUNCTION__, remote_active_sdp.toString().c_str());
    
    auto updated = session_->Update(remote_active_sdp);
    if (!updated)
        log_.Warning("%s: update session failed!", __FUNCTION__);
}

void SipCall::OnCallStateChanged()
{
    log_.Trace("%s: enter.", __FUNCTION__);

    auto call_state = getCallState();
    if (call_state == CallState::Disconnected)
    {
        session_->Close();
    }
    
    OnCallStateChangedEvent(call_state);
}

std::unique_ptr<sdp::SessionDescription> SipCall::CreateSessionDescription()
{
    std::mutex mtx;
    std::condition_variable signal;
    bool result_ready{false};
    std::unique_ptr<sdp::SessionDescription> sdp;
    auto create_sdp_cb = [&mtx, &signal, &result_ready, &sdp](std::unique_ptr<sdp::SessionDescription> desc)
    {
        std::lock_guard<std::mutex> _(mtx);
        sdp = std::move(desc);
        result_ready = true;
        signal.notify_one();
    };
    
    session_->CreateSessionDescription(std::move(create_sdp_cb));

    {
        std::unique_lock<std::mutex> lock(mtx);
        signal.wait(lock, [&result_ready]{ return result_ready; });
    }

    return sdp;
}

CallState SipCall::toCallState(pjsip_inv_state state)
{
    switch (state) {
        case PJSIP_INV_STATE_NULL:
            return CallState::Null;
        case PJSIP_INV_STATE_CALLING:
            return CallState::Calling;
        case PJSIP_INV_STATE_INCOMING:
            return CallState::Incoming;
        case PJSIP_INV_STATE_CONNECTING:
            return CallState::Connecting;
        case PJSIP_INV_STATE_CONFIRMED:
            return CallState::Confirmed;
        case PJSIP_INV_STATE_DISCONNECTED:
            return CallState::Disconnected;
        default:
            throw;
    }
}
