#ifndef LOG_CONFIGURATOR_H
#define LOG_CONFIGURATOR_H

#include "log/loglevel.h"
#include <type_traits>
#include <string>

namespace zt
{
    namespace log
    {
        enum OutputType
        {
            CONSOLE = 0x1,
            FILE    = 0x2
        };

        inline OutputType operator | (OutputType lhs, OutputType rhs)
        {
            using T = std::underlying_type_t <OutputType>;
            return static_cast<OutputType>(static_cast<T>(lhs) | static_cast<T>(rhs));
        }

        inline OutputType& operator |= (OutputType& lhs, OutputType rhs)
        {
            lhs = lhs | rhs;
            return lhs;
        }

        class Configurator
        {
        public:
            static void Setup(LogLevel level, OutputType output_type,
                                const std::string& file_name_prefix = "");
        };
    } //log
} //zt

#endif //LOG_CONFIGURATOR_H
