//
//  call_slot.h
//  sip
//
//  Created by Vyacheslav Demidyuk on 12/12/20.
//

#ifndef CALL_SLOT_H
#define CALL_SLOT_H

#include <memory>

namespace zt
{
    namespace sip
    {
        class SipCall;
        
        class CallSlot
        {
        public:
            CallSlot(uint32_t id)
            :id_(id)
            {
            }
            
            uint32_t getId() const
            {
                return id_;
            }
            
            std::shared_ptr<SipCall> getCall()
            {
                auto call = call_.lock();
                return call;
            }
            
            void setCall(std::weak_ptr<SipCall> call)
            {
                call_ = call;
            }
            
            bool isEmpty()
            {
                auto call = call_.lock();
                return call == nullptr;
            }
            
        private:
            uint32_t                id_;
            std::weak_ptr<SipCall>     call_;
        };
    }
}

#endif /* CALL_SLOT_H */
