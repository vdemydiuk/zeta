#---------------------------------------------------------------------------
# Get and build pjsip

message( "External project - PJSIP" )

set( PJSIP_Configure_Command )

# Note: It IS important to download different files on different OS's:
# on Unix-like systems, we need the file persmissions (only available in the .tar.gz),
# while on Windows, we need CR/LF line feeds (only available in the .zip)

if( UNIX )
  set( PJSIP_url "https://github.com/pjsip/pjproject/archive/2.10.tar.gz")
#  set( PJSIP_url "file:///Volumes/MacHD/projects/2.10.tar.gz")
  set( PJSIP_Configure_Command ./configure )
  set( PJSIP_make_Command make dep && make clean && make -j8 )
  set( PJSIP_install_Command make install )
  set( CONFIGURE_ARGS
    "--disable-ssl"
    "--disable-shared"
    "--disable-resample"
    "--disable-sound"
    "--disable-video"
    "--disable-ext-sound"
    "--disable-speex-aec"
    "--disable-g711-codec"
    "--disable-l16-codec"
    "--disable-gsm-codec"
    "--disable-g722-codec"
    "--disable-g7221-codec"
    "--disable-speex-codec"
    "--disable-ilbc-codec"
    "--disable-libsamplerate"
    "--disable-resample-dll"
    "--disable-sdl"
    "--disable-ffmpeg"
    "--disable-v4l2"
    "--disable-openh264"
    "--disable-ipp"
    "--disable-opencore-amr"
    "--disable-silk"
    "--disable-opus"
    "--disable-libyuv"
    "ac_cv_lib_uuid_uuid_generate=no")
else()
  message ( FATAL_ERROR "PJSIP is not configured for Windows" )
  
#    set(PATCH_CMD "${CARRIER_HOST_TOOLS_DIR}/usr/bin/patch.exe")
#    set( PJSIP_url "https://github.com/pjsip/pjproject/archive/2.10.zip")
endif()

if(CMAKE_GENERATOR STREQUAL Xcode)
  # xcode builds pjlib with error. It puts object files of pjlib and pjlib-test in same folder.
  
  set( PATCH_CMD "patch" -s -p1 < ${CMAKE_CURRENT_LIST_DIR}/pjsip.patch)
else()
  set( PATCH_CMD "" )
endif()

if( APPLE )
  execute_process(
                  COMMAND gcc -dumpmachine
                  OUTPUT_VARIABLE PJLIB_SUFFIX
                  ERROR_QUIET
                  OUTPUT_STRIP_TRAILING_WHITESPACE )
else()
  set ( PJLIB_SUFFIX x86_64-unknown-linux-gnu )
endif()



set( PJSIP_LIBRARY_DIRS ${INSTALL_DEPENDENCIES_DIR}/lib )
set ( PJSIP_LIBRARIES
  ${PJSIP_LIBRARY_DIRS}/libpjsua2-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libpjsua-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libpjsip-ua-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libpjsip-simple-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libpjsip-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libpjmedia-codec-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libpjmedia-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libsrtp-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libpj-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libpjmedia-audiodev-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libpjmedia-videodev-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libpjnath-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libwebrtc-${PJLIB_SUFFIX}.a
  ${PJSIP_LIBRARY_DIRS}/libpjlib-util-${PJLIB_SUFFIX}.a )

ExternalProject_Add(pjsip
  PREFIX ${CMAKE_CURRENT_BINARY_DIR}/pjsip
  BUILD_IN_SOURCE 1
  URL ${PJSIP_url}
  UPDATE_COMMAND ""
  PATCH_COMMAND ${PATCH_CMD}
  CONFIGURE_COMMAND ${PJSIP_Configure_Command} ${CONFIGURE_ARGS} --prefix=${INSTALL_DEPENDENCIES_DIR}
  BUILD_COMMAND ${PJSIP_make_Command}
  INSTALL_COMMAND ${PJSIP_install_Command}
  OUTPUT ${PJSIP_LIBRARIES}
)

if( WIN32 )
#  set( PJSIP_INCLUDE_DIR ${INSTALL_DEPENDENCIES_DIR}/include/pjsip-2_10 )
#  set( PJSIP_ROOT ${INSTALL_DEPENDENCIES_DIR} )
else()
  set( PJSIP_INCLUDE_DIR ${INSTALL_DEPENDENCIES_DIR}/include )
endif()

if( UNIX )
  if( APPLE )
    execute_process(
                    COMMAND gcc -dumpmachine
                    OUTPUT_VARIABLE PJLIB_SUFFIX
                    ERROR_QUIET
                    OUTPUT_STRIP_TRAILING_WHITESPACE )
  else()
    set ( PJLIB_SUFFIX x86_64-unknown-linux-gnu )
  endif()

  if( APPLE )
    set ( PJSIP_LIBRARIES
        "-framework CoreFoundation"
        "-framework Security"
        ${PJSIP_LIBRARIES} )
  endif()
else()
  # TODO windows
endif()
