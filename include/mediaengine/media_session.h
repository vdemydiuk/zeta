#ifndef MEDIA_SESSION_H
#define MEDIA_SESSION_H

#include "log/logger.h"
#include "common/session.h"
#include "api/peer_connection_interface.h"

namespace zt
{
    class PeerConnectionFactory;

    class CreateSessionDescRequest : public webrtc::CreateSessionDescriptionObserver
    {
        typedef std::function<void (webrtc::SessionDescriptionInterface*)> Callback;
    public:
        CreateSessionDescRequest(rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection,
                        rtc::scoped_refptr<webrtc::SetLocalDescriptionObserverInterface> observer,
                        Callback callback);

        void OnIceGatheringComplete();

    private:
        // CreateSessionDescriptionObserver implementation.
        void OnSuccess(webrtc::SessionDescriptionInterface* desc) override;
        void OnFailure(webrtc::RTCError error) override;

        rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection_;
        rtc::scoped_refptr<webrtc::SetLocalDescriptionObserverInterface> set_desc_observer;
        Callback callback_;
        webrtc::SessionDescriptionInterface* desc_;
    };

    class MediaSession : public Session,
                        public webrtc::PeerConnectionObserver
    {
    public:
        MediaSession(PeerConnectionFactory& peer_connection_factory);
        ~MediaSession() override;
        
        // Session implementation
        void CreateSessionDescription(CreateSessionCallback callback) override;
        bool Update(const sdp::SessionDescription& remote_sdp) override;
        void Close() override;
        
    private:
        // PeerConnectionObserver implementation.
        void OnSignalingChange(
            webrtc::PeerConnectionInterface::SignalingState new_state) override {}
        void OnAddTrack(
            rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver,
            const std::vector<rtc::scoped_refptr<webrtc::MediaStreamInterface>>&
                streams) override {}
        void OnRemoveTrack(
            rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver) override {}
        void OnDataChannel(
            rtc::scoped_refptr<webrtc::DataChannelInterface> channel) override {}
        void OnRenegotiationNeeded() override {}
        void OnIceConnectionChange(
            webrtc::PeerConnectionInterface::IceConnectionState new_state) override {}
        void OnIceGatheringChange(
            webrtc::PeerConnectionInterface::IceGatheringState new_state) override;
        void OnIceCandidate(const webrtc::IceCandidateInterface* candidate) override;
        void OnIceConnectionReceivingChange(bool receiving) override {}

        log::Logger log_;
        rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection_;
        rtc::scoped_refptr<CreateSessionDescRequest> create_session_desc_request_;
    };
}

#endif //MEDIA_SESSION_H