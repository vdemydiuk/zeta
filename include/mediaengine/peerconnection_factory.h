//
//  peerconnection_factory.h
//  mediaengine
//
//  Created by Vyacheslav Demidyuk on 11/18/21.
//

#ifndef PEER_CONNECTION_FACTORY_H
#define PEER_CONNECTION_FACTORY_H

#include "log/logger.h"
#include "api/scoped_refptr.h"

namespace rtc 
{
    class Thread;
}

namespace webrtc
{
    class PeerConnectionFactoryInterface;
    class PeerConnectionInterface;
    class PeerConnectionObserver;
}

namespace zt
{
    class PeerConnectionFactory
    {
    public:
        PeerConnectionFactory();
        ~PeerConnectionFactory();

        rtc::scoped_refptr<webrtc::PeerConnectionInterface> CreatePeerConnection(webrtc::PeerConnectionObserver* observer);

    private:
        log::Logger log_;
        std::unique_ptr<rtc::Thread> network_thread_;
        std::unique_ptr<rtc::Thread> worker_thread_;
        std::unique_ptr<rtc::Thread> signaling_thread_;
        rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> peer_connection_factory_;
    };
}

#endif /* PEER_CONNECTION_FACTORY_H */