//
//  media_type.h
//  zt
//
//  Created by Vyacheslav Demidyuk on 3/1/21.
//

#ifndef SDP_MEDIA_TYPE_H
#define SDP_MEDIA_TYPE_H

namespace zt
{
    namespace sdp
    {
        enum class MediaType
        {
            AUDIO,
            VIDEO,
            TEXT,
            APPLICATION,
            MESSAGE,
            UNKNOWN
        };
        
        static std::string toString(MediaType type)
        {
            switch (type) {
                case MediaType::AUDIO: return "audio";
                case MediaType::VIDEO: return "video";
                case MediaType::TEXT: return "text";
                case MediaType::APPLICATION: return "application";
                case MediaType::MESSAGE: return "message";
                case MediaType::UNKNOWN: return "unknown";
                default:
                    throw;
            }
        }
    }
}

#endif /* SDP_MEDIA_TYPE_H */
