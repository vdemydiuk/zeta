//
//  bandwidth.h
//  zt
//
//  Created by Vyacheslav Demidyuk on 10/18/20.
//

#ifndef SDP_BANDWIDTH_H
#define SDP_BANDWIDTH_H

namespace zt
{
    namespace sdp
    {
        enum class BandwidthType
        {
            CT,
            AS,
            TIAS,
            RR,
            RS
        };
        
        static std::string toString(BandwidthType type)
        {
            switch (type) {
                case BandwidthType::CT: return "CT";
                case BandwidthType::AS: return "AS";
                case BandwidthType::TIAS: return "TIAS";
                case BandwidthType::RR: return "RR";
                case BandwidthType::RS: return "RS";
                default:
                    throw;
            }
        }
        
        class Bandwidth
        {
        public:
            Bandwidth(BandwidthType type, uint32_t bandwidth)
            :type_(type)
            ,bandwidth_(bandwidth)
            {
            }

            BandwidthType getType() const { return type_; }
            uint32_t getBandwidth() const { return bandwidth_; }
            
            std::string toString() const { return "b=" + sdp::toString(type_) + ":" + std::to_string(bandwidth_) + "\r\n"; }

        private:
            BandwidthType   type_;
            uint32_t        bandwidth_;
        };
    }
}

#endif /* SDP_BANDWIDTH_H */
