//
//  media_session_provider.h
//  mediaengine
//
//  Created by Vyacheslav Demidyuk on 4/28/21.
//

#ifndef MEDIA_SESSION_PROVIDER_H
#define MEDIA_SESSION_PROVIDER_H

#include "log/logger.h"
#include "common/session_provider.h"

namespace rtc 
{
    class Thread;
}

namespace zt
{
    class PeerConnectionFactory;

    class MediaSessionProvider : public SessionProvider
    {
    public:
        MediaSessionProvider();
        ~MediaSessionProvider();

        bool Create();
        void Destroy();

        bool isCreated() { return peer_connection_factory_ != nullptr; }

        std::unique_ptr<Session> CreateSession() override;
        
    private:
        log::Logger log_;
        std::unique_ptr<PeerConnectionFactory> peer_connection_factory_;
    };
}

#endif /* MEDIA_SESSION_PROVIDER_H */
