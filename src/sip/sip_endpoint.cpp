#include "sip/sip_endpoint.h"
#include "sip/sip_call.h"
#include "sip/call_slot.h"
#include "sip/call_slot_collection.h"
#include "common/session.h"
#include "common/session_provider.h"

using namespace zt;
using namespace zt::sip;

static const char* UNSUPPORTED_OPERATION = "Unsupported Operation";
static const char* UNABLE_CREATE_DIALOG = "Unable to create dialog";
static const char* UNABLE_ALLOCATE_CALL = "Unable allocate call";

log::Logger SipEndpoint::log_{"PJSIP"};
SipEndpoint* SipEndpoint::instance_{nullptr};

SipEndpoint::SipEndpoint(SessionProvider& provider)
:session_provider_(provider)
{
}

SipEndpoint::~SipEndpoint()
{
    Destroy();
}

bool SipEndpoint::Create(const Config& config)
{
    log_.Trace("%s: entry", __FUNCTION__);
    
    if (sip_endpt_)
    {
        log_.Info("%s: PJSIP already initialized.", __FUNCTION__);
        return false;
    }

    instance_ = this;

   /* Must init PJLIB first */
    auto status = pj_init();
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: PJLIB initialization failed! Status = %d", __FUNCTION__, status);
        return false;
    }

    /* init pj log */
    pj_log_set_log_func( &LogWriter );
    pj_log_set_decor(PJ_LOG_HAS_SENDER);
    
    /* init PJLIB-UTIL: */
    status = pjlib_util_init();
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: PJLIB-UTIL initialization failed! Status = %d", __FUNCTION__, status);
        pj_shutdown();
        return false;
    }

    auto pj_release = [this](const std::string& msg, pj_status_t status) 
    {
        log_.Error("Create: %s! Status = %d", msg.c_str(), status);
        ReleasePjLib();
    };

    /* Must create a pool factory before we can allocate any memory. */
    pj_caching_pool_init(&cp_, &pj_pool_factory_default_policy, 0);
    
    /* Create application pool for misc. */
    pool_ = pj_pool_create(&cp_.factory, "app", 1000, 1000, NULL);
    
    /* Create the endpoint: */
    status = pjsip_endpt_create(&cp_.factory, pj_gethostname()->ptr,
                                &sip_endpt_);
    if (status != PJ_SUCCESS)
    {
        pj_release("Create sip enpoint failed", status);
        return false;
    }
    
    /* Initialize SIP transport. */
    int af = (config.UseIPv6) ? pj_AF_INET6() : pj_AF_INET();
    pjsua_transport_config trans_config;
    pjsua_transport_config_default(&trans_config);
    pj_cstr(&trans_config.bound_addr, config.LocalAddress.c_str());
    trans_config.port = config.Port;

    status = InitSipTransport(config.SipUDP, trans_config, af);
    if (status != PJ_SUCCESS)
    {
        pj_release("SIP transport initialization failed", status);
        return false;
    }
    
    /*
     * Init transaction layer.
     * This will create/initialize transaction hash tables etc.
     */
    status = pjsip_tsx_layer_init_module(sip_endpt_);
    if (status != PJ_SUCCESS)
    {
        pj_release("Transaction layer initialization failed", status);
        return false;
    }
    
    /*  Initialize UA layer. */
    status = pjsip_ua_init_module( sip_endpt_, NULL );
    if (status != PJ_SUCCESS)
    {
        pj_release("UA layer initialization failed", status);
        return false;
    }
    
    /* Initialize 100rel support */
    status = pjsip_100rel_init_module(sip_endpt_);
    if (status != PJ_SUCCESS)
    {
        pj_release("UA layer initialization failed", status);
        return false;
    }
    
    /*  Init invite session module. */
    {
        pjsip_inv_callback inv_cb;
        
        /* Init the callback for INVITE session: */
        pj_bzero(&inv_cb, sizeof(inv_cb));
        inv_cb.on_state_changed = &CallOnStateChanged;
        inv_cb.on_new_session = &CallOnForked;
        inv_cb.on_media_update = &CallOnMediaUpdate;
        
        /* Initialize invite session module:  */
        status = pjsip_inv_usage_init(sip_endpt_, &inv_cb);
        if (status != PJ_SUCCESS)
        {
            pj_release("Invite session module initialization failed", status);
            return false;
        }
    }
    
    /* Register our module to receive incoming requests. */
    status = pjsip_endpt_register_module( sip_endpt_, &mod_siprtp_);
    if (status != PJ_SUCCESS)
    {
        pj_release("Invite session module initialization failed", status);
        return false;
    }
    
    call_slots_ = std::make_unique<CallSlotCollection>(mod_siprtp_.id);
    
    /* Start worker threads */
    is_threads_run_ = true;
    for (int i = 0; i < config.WorkingThreadCount; ++i)
    {
        pj_thread_t* thread;
        auto success = pj_thread_create( pool_, "sip", &SipEndpoint::SipWorkerThread, NULL, 0, 0, &thread);
        if (success == PJ_SUCCESS)
            threads_.push_back(thread);
    }
    
    log_.Debug("%s: initalized", __FUNCTION__);

    is_created_ = true;
    return true;
}

void SipEndpoint::Destroy()
{
    log_.Trace("%s: entry", __FUNCTION__);
    
    if (is_created_)
    {
        is_threads_run_ = false;
        for (auto thread : threads_)
        {
            pj_thread_join(thread);
            pj_thread_destroy(thread);
        }
        threads_.clear();
    
        call_slots_.release();
        ReleasePjLib();

        is_created_ = false;
    }
    
    log_.Debug("%s: destroyed", __FUNCTION__);
}

void SipEndpoint::LogWriter(int level, const char *buffer, int len)
{
    /**
    * The level conventions:
    *  - 0: fatal error
    *  - 1: error
    *  - 2: warning
    *  - 3: info
    *  - 4: debug
    *  - 5: trace
    *  - 6: more detailed trace
    **/
    switch (level)
    {
    case 0:
        log_.Fatal(buffer);
        break;
    case 1:
        log_.Error(buffer);
        break;
    case 2:
        log_.Warning(buffer);
        break;
    case 3:
        log_.Info(buffer);
        break;
    case 4:
        log_.Debug(buffer);
        break;
    case 5:
        log_.Trace(buffer);
        break;
    default:
        break;
    }
}

void SipEndpoint::CallOnStateChanged( pjsip_inv_session *inv,
                                     pjsip_event *e)
{
    log_.Trace("%s: entry", __FUNCTION__);
    
    instance_->ProcessCallState(inv);
}

void SipEndpoint::CallOnForked(pjsip_inv_session *inv, pjsip_event *e)
{
    PJ_UNUSED_ARG(inv);
    PJ_UNUSED_ARG(e);
    
    PJ_TODO( HANDLE_FORKING );
    
    log_.Trace("%s: entry", __FUNCTION__);
}

void SipEndpoint::CallOnMediaUpdate( pjsip_inv_session *inv,
                                    pj_status_t status)
{
    log_.Trace("%s: entry. status = %d", __FUNCTION__, status);
    
    //TODO: check call state on hangup
    
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: SDP negotiation has failed. status = %d", __FUNCTION__, status);
        
        /* Disconnect call if we're not in the middle of initializing an
         * UAS dialog and if this is not a re-INVITE
         */
        if (inv->state != PJSIP_INV_STATE_NULL &&
            inv->state != PJSIP_INV_STATE_CONFIRMED)
        {
            pjsip_tx_data *tdata;
            auto st = pjsip_inv_end_session(inv, PJSIP_SC_UNSUPPORTED_MEDIA_TYPE, NULL, &tdata);
            if (st != PJ_SUCCESS)
                return;
            pjsip_inv_send_msg(inv, tdata);
        }
        
        return;
    }
    
    instance_->ProcessMediaUpdate(inv);
}

pj_bool_t SipEndpoint::OnRxRequest( pjsip_rx_data *rdata )
{
    log_.Trace("%s: entry", __FUNCTION__);
    
    /* Ignore strandled ACKs (must not send respone) */
    if (rdata->msg_info.msg->line.req.method.id == PJSIP_ACK_METHOD)
        return PJ_FALSE;
    
    /* Respond (statelessly) any non-INVITE requests with 500  */
    if (rdata->msg_info.msg->line.req.method.id != PJSIP_INVITE_METHOD)
    {
        pj_str_t reason;
        pj_cstr(&reason, UNSUPPORTED_OPERATION);
        pjsip_endpt_respond_stateless(instance_->sip_endpt_, rdata,
                                      500, &reason,
                                      NULL, NULL);
    }
    else
    {
        /* Handle incoming INVITE */
        instance_->ProcessIncomingCall(rdata);
    }
    return PJ_TRUE;
}

pj_bool_t SipEndpoint::OnRxResponse(pjsip_rx_data *rdata)
{
    log_.Trace("%s: entry", __FUNCTION__);
    return PJ_TRUE;
}

std::shared_ptr<Call> SipEndpoint::MakeCall(const std::string& dst_uri)
{
    log_.Trace("%s: entry. dst_uri = %s", __FUNCTION__, dst_uri.c_str());

    if (!is_created_)
    {
        log_.Error("%s: SIP endpoint is initialized!", __FUNCTION__);
        return nullptr;
    }
    
    if (VerifySipUrl(dst_uri.c_str()) == false)
    {
        log_.Error("%s: Destination URI is incorrect: %s", __FUNCTION__, dst_uri.c_str());
        return nullptr;
    }
    
    pj_str_t pjstr_dst_uri;
    pj_cstr(&pjstr_dst_uri, dst_uri.c_str());

    std::string uas_contact = CreateUasContact();

    /* Create UAC dialog */
    pjsip_dialog *dlg{nullptr};
    pj_str_t pj_local_uri;
    pj_cstr(&pj_local_uri, uas_contact.c_str());
    auto status = pjsip_dlg_create_uac( pjsip_ua_instance(),
                                       &pj_local_uri,      /* local URI        */
                                       &pj_local_uri,      /* local Contact    */
                                       &pjstr_dst_uri,     /* remote URI       */
                                       &pjstr_dst_uri,     /* remote target    */
                                       &dlg);              /* dialog           */
    
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Failed to create UAC dialog (%d).", __FUNCTION__, status);
        return nullptr;
    }
    
    /* Create the INVITE session. */
    pjsip_inv_session* inv;
    status = pjsip_inv_create_uac(dlg, NULL, 0, &inv);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Failed to create INVITE session. status = %d", __FUNCTION__, status);
        pjsip_dlg_terminate(dlg);
        return nullptr;
    }
    
    /* Make call */
    auto session = session_provider_.CreateSession();
    if (session == nullptr)
    {
        log_.Error("%s: Failed to create session.", __FUNCTION__);
        pjsip_inv_terminate(inv, 500, PJ_FALSE);
        return nullptr;
    }

    auto call = std::make_shared<SipCall>(inv, std::move(session));
    
    /* Acquire call slot */
    auto slot = call_slots_->AcquireSlot(call);
    if (slot == nullptr)
    {
        log_.Error("%s: Failed to acquire call slot.", __FUNCTION__);
        pjsip_inv_terminate(inv, 500, PJ_FALSE);
        return nullptr;
    }
    
    /* Attach slot to invite session */
    inv->mod_data[mod_siprtp_.id] = slot;

    auto success = call->Invite();
    if (!success)
    {
        log_.Error("%s: Failed invite.", __FUNCTION__);
        return nullptr;
    }

    return call;
}

int SipEndpoint::SipWorkerThread(void *arg)
{
    log_.Trace("%s: entry", __FUNCTION__);
    
    while (instance_->is_threads_run_)
    {
        pj_time_val timeout = {0, 10};
        pjsip_endpt_handle_events(instance_->sip_endpt_, &timeout);
    }
    
    log_.Trace("%s: exit", __FUNCTION__);

    return 0;
}

void SipEndpoint::ProcessIncomingCall(pjsip_rx_data *rdata)
{
    log_.Trace("%s: entry", __FUNCTION__);
    
    /* Verify that we can handle the request. */
    unsigned options = 0;
    pjsip_tx_data *tdata;
    pj_status_t status = pjsip_inv_verify_request(rdata, &options, NULL, NULL,
                                                  sip_endpt_, &tdata);
    if (status != PJ_SUCCESS)
    {
        log_.Warning("%s: The incoming INVITE request can't be handled.", __FUNCTION__);
        
        if (tdata)
        {
            pjsip_response_addr res_addr;
            
            pjsip_get_response_addr(tdata->pool, rdata, &res_addr);
            pjsip_endpt_send_response(sip_endpt_, &res_addr, tdata,
                                      NULL, NULL);
        }
        else
        {
            /* Respond with 500 (Internal Server Error) */
            pjsip_endpt_respond_stateless(sip_endpt_, rdata, 500, NULL,
                                          NULL, NULL);
        }
        return;
    }
    
    /* Parse SDP from incoming request */
    pjmedia_sdp_session* offer{nullptr};
    if (rdata->msg_info.msg->body)
    {
        pjsip_rdata_sdp_info *sdp_info;
        
        sdp_info = pjsip_rdata_get_sdp_info(rdata);
        offer = sdp_info->sdp;
        
        status = sdp_info->sdp_err;
        if (status == PJ_SUCCESS && sdp_info->sdp == nullptr &&
            !PJSIP_INV_ACCEPT_UNKNOWN_BODY)
        {
            if (sdp_info->body.ptr == NULL)
            {
                status = PJSIP_ERRNO_FROM_SIP_STATUS(
                                                     PJSIP_SC_UNSUPPORTED_MEDIA_TYPE);
            }
            else
            {
                status = PJSIP_ERRNO_FROM_SIP_STATUS(PJSIP_SC_NOT_ACCEPTABLE);
            }
        }
        
        if (status != PJ_SUCCESS)
        {
            pjsip_hdr hdr_list;
            
            /* Check if body really contains SDP. */
            if (sdp_info->body.ptr == NULL)
            {
                /* Couldn't find "application/sdp" */
                pjsip_accept_hdr *acc;
                
                log_.Error("%s: Unknown Content-Type in incoming INVITE (status = %d)", __FUNCTION__, status);
                
                /* Add Accept header to response */
                acc = pjsip_accept_hdr_create(rdata->tp_info.pool);
                if (acc == nullptr)
                {
                    log_.Warning("%s: failed add accept header to response.", __FUNCTION__);
                }
                else
                {
                    acc->values[acc->count++] = pj_str((char*)"application/sdp");
                    pj_list_init(&hdr_list);
                    pj_list_push_back(&hdr_list, acc);
                    
                    pjsip_endpt_respond(sip_endpt_, NULL, rdata,
                                        PJSIP_SC_UNSUPPORTED_MEDIA_TYPE,
                                        NULL, &hdr_list, NULL, NULL);
                }
            }
            else
            {
                const pj_str_t reason = pj_str((char*)"Bad SDP");
                pjsip_warning_hdr *w;
                
                log_.Error("%s: Bad SDP in incoming INVITE (status = %d)", __FUNCTION__, status);
                
                w = pjsip_warning_hdr_create_from_status(rdata->tp_info.pool,
                                                         pjsip_endpt_name(sip_endpt_),
                                                         status);
                pj_list_init(&hdr_list);
                pj_list_push_back(&hdr_list, w);
                
                pjsip_endpt_respond(sip_endpt_, NULL, rdata, 400,
                                    &reason, &hdr_list, NULL, NULL);
            }
            return;
        }
        
        /* Do quick checks on SDP before passing it to transports. More elabore
         * checks will be done in pjsip_inv_verify_request2() below.
         */
        if ((offer) && (offer->media_count==0))
        {
            log_.Error("%s: Missing media in SDP", __FUNCTION__);
            
            pj_str_t reason; 
            pj_cstr(&reason, "Missing media in SDP");
            pjsip_endpt_respond(sip_endpt_, NULL, rdata, 400, &reason,
                                NULL, NULL, NULL);
            return;
        }
    }

    std::string uas_contact = CreateUasContact();
    
    /* Create UAS dialog */
    pjsip_dialog *dlg;
    pj_str_t local_contact;
    pj_cstr(&local_contact, uas_contact.c_str());
    status = pjsip_dlg_create_uas_and_inc_lock( pjsip_ua_instance(), rdata,
                                               &local_contact, &dlg);
    if (status != PJ_SUCCESS)
    {
        log_.Warning("%s: Failed to create UAS dialog (%d).", __FUNCTION__, status);
        
        pj_str_t reason; 
        pj_cstr(&reason, UNABLE_CREATE_DIALOG);
        pjsip_endpt_respond_stateless( sip_endpt_, rdata,
                                      500, &reason,
                                      NULL, NULL);
        return;
    }
    
    /* Create UAS invite session */
    pjsip_inv_session* inv;
    status = pjsip_inv_create_uas(dlg, rdata, nullptr, 0, &inv);
    if (status != PJ_SUCCESS)
    {
        log_.Warning("%s: Failed to create UAS invite session.", __FUNCTION__);
        pjsip_dlg_create_response(dlg, rdata, 500, NULL, &tdata);
        pjsip_dlg_send_response(dlg, pjsip_rdata_get_tsx(rdata), tdata);
        return;
    }
    
    bool success{false};
    /* Create the 100 response. */
    status = pjsip_inv_initial_answer(inv, rdata, 100, nullptr, nullptr, &tdata);
    if (status != PJ_SUCCESS)
    {
        log_.Warning("%s: Failed to create initial answer.", __FUNCTION__);
    }
    else
    {
        /* Send the 100 response. */
        status = pjsip_inv_send_msg(inv, tdata);
        if (status != PJ_SUCCESS)
        {
            log_.Warning("%s: Failed to send initial answer.", __FUNCTION__);
        }
        else
        {
            /* Make call */
            auto session = session_provider_.CreateSession();
            if (session == nullptr)
            {
                log_.Error("%s: Failed to create session.", __FUNCTION__);
            }
            else
            {
                auto call = std::make_shared<SipCall>(inv, std::move(session));
                
                /* Acquire call slot*/
                auto slot = call_slots_->AcquireSlot(call);
                if (call == nullptr)
                {
                    log_.Warning("%s: Failed to acquire call slot.", __FUNCTION__);
                }
                else
                {
                    /* Convert pjmedia sdp session to string */
                    char buffer[8000];
                    auto size = pjmedia_sdp_print(offer, buffer, sizeof(buffer));
                    if (size > 0)
                    {
                        buffer[size] = '\0';
                        log_.Debug("%s: received sdp offer\n%s", __FUNCTION__, buffer);
                    }

                    /* Attach slot to invite session */
                    inv->mod_data[mod_siprtp_.id] = slot;
                    
                    /* Rise event incoming call */
                    OnIncomingCallEvent(call);

                    success = true;
                }                
            }
        }
    }
    
    if (!success)
    {
        pjsip_dlg_create_response(dlg, rdata, 500, NULL, &tdata);
        pjsip_dlg_send_response(dlg, pjsip_rdata_get_tsx(rdata), tdata);

        pjsip_inv_terminate(inv, 500, PJ_FALSE);
        log_.Warning("%s: Invite terminated.", __FUNCTION__);
    }
    
    pjsip_dlg_dec_lock(dlg);
}

void SipEndpoint::ProcessMediaUpdate(pjsip_inv_session *inv)
{
    log_.Trace("%s: entry", __FUNCTION__);
    
    /* Get local SDP */
    const pjmedia_sdp_session *local_sdp;
    auto status = pjmedia_sdp_neg_get_active_local(inv->neg, &local_sdp);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Unable to retrieve currently active local SDP. status = %d",
                   __FUNCTION__, status);
        return;
    }
    
    /* Get remote SDP */
    const pjmedia_sdp_session *remote_sdp;
    status = pjmedia_sdp_neg_get_active_remote(inv->neg, &remote_sdp);
    if (status != PJ_SUCCESS)
    {
        log_.Error("%s: Unable to retrieve currently active remote SDP. status = %d",
                   __FUNCTION__, status);
        return;
    }
    
    auto slot = static_cast<CallSlot*>(inv->mod_data[mod_siprtp_.id]);
    if (slot)
    {
        auto call = slot->getCall();
        if (call)
            call->OnNegotiationCompleted(local_sdp, remote_sdp);
    }
}

void SipEndpoint::ProcessCallState(pjsip_inv_session *inv)
{
    log_.Trace("%s: entry", __FUNCTION__);
    
    auto slot = static_cast<CallSlot*>(inv->mod_data[mod_siprtp_.id]);
    if (slot)
    {
        auto call = slot->getCall();
        if (call)
            call->OnCallStateChanged();
    }
}

void SipEndpoint::ReleasePjLib()
{
    if (sip_endpt_)
    {
        pjsip_endpt_destroy(sip_endpt_);
        sip_endpt_ = nullptr;
    }
    pj_pool_release(pool_);
    pj_caching_pool_destroy(&cp_);
    pj_shutdown();

    local_addr_.clear();
    local_port_ = 0;
    transport_type_.clear();
}

pj_status_t SipEndpoint::InitSipTransport(bool udp, pjsua_transport_config& config, int af)
{
    if (udp)
    {
        pjsip_udp_transport_cfg udp_cfg;
        pjsip_transport *udp;

        pjsip_udp_transport_cfg_default(&udp_cfg, af);

        if (config.port)
            pj_sockaddr_set_port(&udp_cfg.bind_addr, (pj_uint16_t)config.port);

        if (config.bound_addr.slen)
        {
            auto status = pj_sockaddr_set_str_addr(udp_cfg.af, 
                            &udp_cfg.bind_addr,
                            &config.bound_addr);
            if (status != PJ_SUCCESS)
            {
                log_.Warning("%s: Unable to resolve IP interface.", __FUNCTION__);
                return status;
            }
        }

        auto status = pjsip_udp_transport_start2(sip_endpt_, &udp_cfg, &udp);
        if (status != PJ_SUCCESS)
        {
            log_.Warning("%s: Unable to start UDP transport.", __FUNCTION__);
            return status;
        }

        local_port_ = udp->local_name.port;
        local_addr_ = pj_strbuf(&udp->local_name.host);
        transport_type_ = "UDP";
    }
    else
    {
        pjsip_tpfactory *tcp;
        pjsip_tcp_transport_cfg tcp_cfg;

        pjsip_tcp_transport_cfg_default(&tcp_cfg, af);

        if (config.port)
            pj_sockaddr_set_port(&tcp_cfg.bind_addr, (pj_uint16_t)config.port);

        if (config.bound_addr.slen)
        {
            auto status = pj_sockaddr_set_str_addr(tcp_cfg.af, 
                                    &tcp_cfg.bind_addr,
                                    &config.bound_addr);
            if (status != PJ_SUCCESS) 
            {
                log_.Warning("%s: Unable to resolve transport bound address.", __FUNCTION__);
                return status;
            }
        }

        auto status = pjsip_tcp_transport_start3(sip_endpt_, &tcp_cfg, &tcp);
        if (status != PJ_SUCCESS)
        {
            log_.Warning("%s: Unable to start TCP transport.", __FUNCTION__);
            return status;
        }

        local_port_ = tcp->addr_name.port;
        local_addr_ = pj_strbuf(&tcp->addr_name.host);
        transport_type_ = "TCP";
    }

    log_.Info("%s: listening %s with address %s:%d", __FUNCTION__, transport_type_.c_str(), local_addr_.c_str(), local_port_);
    
    return PJ_SUCCESS;
}

std::string SipEndpoint::CreateUasContact() const
{
    std::string contact = "<sip:" + local_addr_ + ":" + std::to_string(local_port_);
    if (transport_type_ == "TCP")
        contact += ";transport=tcp>";
    else
        contact += ">";
    return contact;
}

bool SipEndpoint::VerifySipUrl(const std::string& sip_url)
{
    pj_size_t len = sip_url.size();

    if (!len) 
        return false;

    pj_pool_t* pool = pj_pool_create(&cp_.factory, "check%p", 1024, 0, NULL);
    if (!pool) 
        return false;

    char* url = (char*) pj_pool_alloc(pool, len+1);
    pj_ansi_strcpy(url, sip_url.c_str());

    pjsip_uri* p = pjsip_parse_uri(pool, url, len, 0);
    if (!p || (pj_stricmp2(pjsip_uri_get_scheme(p), "sip") != 0 &&
	       pj_stricmp2(pjsip_uri_get_scheme(p), "sips") != 0))
    {
	    p = nullptr;
    }

    pj_pool_release(pool);
    return p != nullptr;
}