#include "gtest/gtest.h"
#include "log/configurator.h"
#include "mediaengine/media_session_provider.h"
#include "mediaengine/media_session.h"
#include "mediaengine/peerconnection_factory.h"
#include "sdp/session_description.h"
#include <mutex>
#include <condition_variable>

using namespace zt;

namespace
{
//-------------------------------------------------------------------------
//  MediaSession
//-------------------------------------------------------------------------

    class MediaSessionTest : public ::testing::Test
    {
    protected:
        void SetUp() override
        { 
        }

        void TearDown() override
        {
        }

        PeerConnectionFactory peer_connection_factory_;
    };

    TEST_F(MediaSessionTest, CreateSessionDescription)
    {
        std::unique_ptr<sdp::SessionDescription> session_description;
        std::mutex mtx;
        std::condition_variable signal;
        bool result_ready{false};
        
        auto cb = [&mtx, &result_ready, &signal, &session_description](std::unique_ptr<sdp::SessionDescription> desc)
        {
            std::lock_guard<std::mutex> _(mtx);
            result_ready = true;
            session_description = std::move(desc);
            signal.notify_one();
        };

        MediaSession media_session(peer_connection_factory_);
        media_session.CreateSessionDescription(std::move(cb));

        {
            std::unique_lock<std::mutex> lock(mtx);
            signal.wait(lock, [&result_ready]{ return result_ready; });
        }

        ASSERT_NE(session_description, nullptr);

        auto media_count = session_description->getMediaCount();
        ASSERT_EQ(media_count, 2);

        auto media_audio = session_description->getMedia(0);
        ASSERT_NE(media_audio, nullptr);
        EXPECT_EQ(media_audio->getMediaType(), sdp::MediaType::AUDIO);
        EXPECT_EQ(media_audio->getProto(), "RTP/AVPF");

        auto media_video = session_description->getMedia(1);
        ASSERT_NE(media_video, nullptr);
        EXPECT_EQ(media_video->getMediaType(), sdp::MediaType::VIDEO);
        EXPECT_EQ(media_video->getProto(), "RTP/AVPF");
    }

    TEST_F(MediaSessionTest, SessionUpdate)
    {
        std::unique_ptr<sdp::SessionDescription> session_description_1;
        std::unique_ptr<sdp::SessionDescription> session_description_2;
        std::mutex mtx;
        std::condition_variable signal;
        bool result_ready_1{false};
        bool result_ready_2{false};

        auto cb_1 = [&mtx, &result_ready_1, &signal, &session_description_1](std::unique_ptr<sdp::SessionDescription> desc)
        {
            std::lock_guard<std::mutex> _(mtx);
            result_ready_1 = true;
            session_description_1 = std::move(desc);
            signal.notify_one();
        };

        auto cb_2 = [&mtx, &result_ready_2, &signal, &session_description_2](std::unique_ptr<sdp::SessionDescription> desc)
        {
            std::lock_guard<std::mutex> _(mtx);
            result_ready_2 = true;
            session_description_2 = std::move(desc);
            signal.notify_one();
        };

        MediaSession media_session_1(peer_connection_factory_);
        MediaSession media_session_2(peer_connection_factory_);

        media_session_1.CreateSessionDescription(std::move(cb_1));
        media_session_2.CreateSessionDescription(std::move(cb_2));

        {
            std::unique_lock<std::mutex> lock(mtx);
            signal.wait(lock, [&result_ready_1, &result_ready_2]{ return result_ready_1 && result_ready_2; });
        }

        ASSERT_NE(session_description_1, nullptr);
        ASSERT_NE(session_description_2, nullptr);

        auto update_success_1 = media_session_1.Update(*session_description_2.get());
        auto update_success_2 = media_session_2.Update(*session_description_1.get());

        EXPECT_TRUE(update_success_1);
        EXPECT_TRUE(update_success_2);
    }

//-------------------------------------------------------------------------
//  MediaSessionProvider
//-------------------------------------------------------------------------

    class MediaSessionProviderTest : public ::testing::Test
    {
    protected:
        MediaSessionProviderTest()
        {
        }

        ~MediaSessionProviderTest() override
        {
        }

        void SetUp() override
        {
            is_me_created_ = media_session_provider_.Create();
        }

        void TearDown() override
        {
            media_session_provider_.Destroy();
        }

        MediaSessionProvider media_session_provider_;
        bool is_me_created_{false};
    };
    
    TEST(MediaSessionProviderCreateDestroyTest, CreateDestroy)
    {
        MediaSessionProvider media_session_provider;
        auto success = media_session_provider.Create();
        EXPECT_TRUE(success);
        EXPECT_TRUE(media_session_provider.isCreated());

        media_session_provider.Destroy();
        EXPECT_FALSE(media_session_provider.isCreated());
    }

    TEST_F(MediaSessionProviderTest, CreateSession)
    {
        ASSERT_TRUE(is_me_created_);
        auto session = media_session_provider_.CreateSession();
        ASSERT_NE(session, nullptr);
    }

}//namespace

int main()
{
    log::Configurator::Setup(log::LogLevel::TRACE, log::OutputType::CONSOLE);
    ::testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}
