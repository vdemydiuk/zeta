//
//  attribute.h
//  zt
//
//  Created by Vyacheslav Demidyuk on 10/12/20.
//

#ifndef SDP_ATTRIBUTE_H
#define SDP_ATTRIBUTE_H

#include <stdint.h>
#include <string>
#include <limits>
#include <vector>
#include <memory>

namespace zt
{
    namespace sdp
    {
        class Attribute
        {
        public:
            virtual ~Attribute() {}

            virtual std::string getAttribute() const = 0;
            virtual std::string getValue() const = 0;

            virtual std::string toString();
        };
        
        class GenericAttribute : public Attribute
        {
        public:
            GenericAttribute(const std::string attr, const std::string& value = "");

            std::string getAttribute() const override { return attribute_; }
            std::string getValue() const override { return value_; }

        private:
            std::string attribute_;
            std::string value_;
        };
        
        class RtpMapAttr : public Attribute
        {
        public:
            RtpMapAttr(uint32_t ptype, const std::string& encoder_name, uint32_t clock_rate, const std::string& parameters = "");

            std::string getAttribute() const override;
            std::string getValue() const override;

            uint32_t getPType() const { return ptype_; }
            std::string getEncoderName() const { return encoder_name_; }
            uint32_t getClockRate() const { return clock_rate_; }
            std::string getParameters() const { return enc_params_; }

        private:
            uint32_t    ptype_;
            std::string encoder_name_;
            uint32_t    clock_rate_;
            std::string enc_params_;
        };
        
        enum class DirectionType
        {
            INACTIVE,
            RECVONLY,
            SENDONLY,
            SENDRECV
        };
        
        class DirectionAttr : public Attribute
        {
        public:
            DirectionAttr(DirectionType direction);

            std::string getAttribute() const override;
            std::string getValue() const override;

            DirectionType getDirection() const { return direction_; }
            void setDirection(DirectionType direction) { direction_ = direction; }

        private:
            DirectionType direction_;
        };
        
        class FormatAttr : public Attribute
        {
        public:
            FormatAttr(uint32_t ptype, const std::string& params);

            std::string getAttribute() const override;
            std::string getValue() const override;

            uint32_t getPType() const { return ptype_; }
            std::string getParameters() const { return params_; }

        private:
            uint32_t    ptype_;
            std::string params_;
        };
        
        enum class FecOrderType
        {
            FEC_SRTP,
            SRTP_FEC
        };
        
        std::string to_string(FecOrderType type);
        
        class CryptoAttr : public Attribute
        {
        public:
            class Lifetime
            {
            public:
                Lifetime(uint32_t value, bool is_exponent = true)
                :value_(value)
                ,is_exponent_(is_exponent)
                {
                }
                
                uint32_t getValue() const { return value_; }
                bool isExponent() const { return is_exponent_; }
                
            private:
                uint32_t    value_;
                bool        is_exponent_;
            };
            
            class MKI
            {
            public:
                MKI(uint32_t value, uint32_t length)
                :value_(value)
                ,length_(length)
                {
                }
                
                uint32_t getValue() const { return value_; }
                uint32_t getLength() const { return length_; }
                
            private:
                uint32_t    value_;
                uint32_t    length_;
            };
            
            class Key
            {
            public:
                Key(const std::string& method, const std::string& salt)
                :method_(method)
                ,salt_(salt)
                {
                }
                
                std::string getMethod() const { return method_; }
                std::string getSalt() const { return salt_; }
                std::shared_ptr<Lifetime> getLifetime() const { return lifetime_; }
                std::shared_ptr<MKI> getMKI() const { return mki_; }
                
                void setLifetime(const std::shared_ptr<Lifetime>& value) { lifetime_ = value; }
                void setMKI(const std::shared_ptr<MKI>& value) { mki_ = value; }
                
                std::string toString();
                
            private:
                std::string                 method_;
                std::string                 salt_;
                std::shared_ptr<Lifetime>   lifetime_;
                std::shared_ptr<MKI>        mki_;
            };
            
            class SrtpParam
            {
            public:
                SrtpParam(const std::string& name)
                :name_(name)
                {
                }
                virtual ~SrtpParam() {}
                
                std::string getName() const { return name_; }
                virtual std::string toString() const { return name_; }
                
            private:
                std::string name_;
                std::string str_value_;
            };
            
            class IntParam : public SrtpParam
            {
            public:
                IntParam(const std::string& name, int value)
                :SrtpParam(name)
                ,value_(value)
                {
                }
                
                int getValue() const { return value_; }
                std::string toString() const override { return getName() + "=" + std::to_string(value_); }
                
            private:
                int value_;
            };
                
            class FecOrderParam : public SrtpParam
            {
            public:
                FecOrderParam(FecOrderType value)
                :SrtpParam("FEC_ORDER")
                ,value_(value)
                {
                }
                
                FecOrderType getValue() const { return value_; }
                std::string toString() const override { return "FEC_ORDER=" + to_string(value_); }
                
            private:
                FecOrderType value_;
            };
            
            CryptoAttr(uint32_t tag, const std::string& crypto_suite);
            
            std::string getAttribute() const override;
            std::string getValue() const override;
            
            uint32_t getTag() const { return tag_; }
            std::string getCryptoSuite() const { return crypto_suite_; }
            
            std::shared_ptr<Key> getKey(uint32_t index) const;
            uint32_t getKeyCount() const { return keys_.size(); }
            void addKey(const std::shared_ptr<Key>& key);
            
            void setTag(uint32_t value) { tag_ = value; }
            void setCryptoSuite(const std::string& value) { crypto_suite_ = value; }
            
            uint32_t getParamCount() const { return params_.size(); }
            std::shared_ptr<SrtpParam> getParam(uint32_t index) const;
            void addParam(const std::shared_ptr<SrtpParam>& param);
            
        private:
            typedef std::vector<std::shared_ptr<CryptoAttr::Key>> CryptoKeyVec;
            typedef std::vector<std::shared_ptr<CryptoAttr::SrtpParam>> CryptoParamVec;
            
            uint32_t        tag_;
            std::string     crypto_suite_;
            CryptoKeyVec    keys_;
            CryptoParamVec  params_;
        };
    }
}

#endif /* SDP_ATTRIBUTE_H */
