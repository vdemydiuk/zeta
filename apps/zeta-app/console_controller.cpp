#include "console_controller.h"

using namespace zt;

ConsoleController::ConsoleController(boost::asio::io_service& service, Callback& callback)
:log_("ConsoleController")
,service_(service)
,stdin_(service, 0)
,stdout_(service, 1)
,input_(&input_buf_)
,output_(&output_buf_)
,callback_(callback)
{
    log_.Trace("ConsoleController ctor");
}

ConsoleController::~ConsoleController()
{
    log_.Trace("ConsoleController dtor");
}

void ConsoleController::Run()
{
    ReadInput();
    callback_.OnRun();
}

void ConsoleController::ReadInput()
{
    input_.clear();

    using namespace std::placeholders;
    boost::asio::async_read_until(stdin_, input_buf_, "\n", std::bind(&ConsoleController::FinishReadInput, this, _1, _2));
}

void ConsoleController::FinishReadInput(const boost::system::error_code& ec, std::size_t len)
{
    if (ec)
    {
        log_.Error("%s: error code %d", __FUNCTION__, ec.value());
        throw boost::system::system_error(ec);
    }

    log_.Trace("%s: len = %d", __FUNCTION__, len);

    std::string command;
    if (input_ >> command)
    {
        log_.Trace("%s: command = %s", __FUNCTION__, command.c_str());
        if (command == "quit")
        {
            stdin_.close();
            callback_.OnQuit();
        }
        else
        {
            WriteOutput("Input 'quit' to exit from application.\n");
        }
    }

    input_.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

void ConsoleController::WriteOutput(const char* message)
{
    output_ << message;

    using namespace std::placeholders;
    boost::asio::async_write(stdout_, output_buf_, std::bind(&ConsoleController::FunishWriteOutput, this, _1, _2));
}

void ConsoleController::FunishWriteOutput(const boost::system::error_code& ec, std::size_t len)
{
    if (ec)
    {
        log_.Error("%s: error code %d", __FUNCTION__, ec.value());
        throw boost::system::system_error(ec);
    }

    log_.Trace("%s: len = %d", __FUNCTION__, len);

    ReadInput();
}
