#include "console_controller.h"
#include "common/call.h"
#include "mediaengine/media_session_provider.h"
#include "log/configurator.h"
#include "sip/sip_endpoint.h"
#include <boost/asio.hpp>

#define SIP_PORT 5068

using namespace zt;
using namespace zt::log;
using namespace zt::sip;

class ConsoleCallback: public ConsoleController::Callback
{
public:
    ConsoleCallback()
    {
    }
    
private:
    void OnRun() override
    {
    }
    
    void OnQuit() override
    {
    }
};

std::vector<std::shared_ptr<Call>> calls_;
boost::asio::io_service io_service_;

bool PlaceCall(SipEndpoint& sip_endpoint, const std::string& addr)
{
    auto call = sip_endpoint.MakeCall(addr);
    if (call)
        calls_.push_back(call);
    return call != nullptr;
}

void OnIncomingCall(const std::shared_ptr<Call>& call)
{
    io_service_.post([c = std::shared_ptr<Call>(call)]()-> void
    {
        calls_.push_back(c);
        c->Answer();
    }); 
}

int main(int argc, char *argv[]) 
{
    Configurator::Setup(LogLevel::TRACE, OutputType::CONSOLE | OutputType::FILE, "zeta");

    Logger log("Main");
    
    MediaSessionProvider media_session_provider;
    SipEndpoint sip_endpoint(media_session_provider);

    sip_endpoint.OnIncomingCallEvent.connect(&OnIncomingCall);

    ConsoleCallback callback;
    ConsoleController console_control(io_service_, callback);

    log.Debug("Started process.");

    std::string call_addr;
    int port{SIP_PORT};
    int ch;

    while ((ch = getopt(argc, argv, "p:c:")) != -1)
    {
        log.Debug("ch = %c, optarg = %s\n", ch, optarg);
        switch( ch )
        {
            case 'p':
                port = std::stoi(optarg);
                break;
            case 'c':
                call_addr = optarg;
                break;
            default:
                break;
        }
    }

    auto created = media_session_provider.Create();
    if (!created)
    {
        log.Error("%s: Failed to create media session provider.", __FUNCTION__);
        return -1;
    }

    Config sip_config{port};
    created = sip_endpoint.Create(sip_config);
    if (!created)
    {
        log.Error("%s: Failed to create SIP endpoint.", __FUNCTION__);
        return -1;
    }

    try
    {
        console_control.Run();
        if (!call_addr.empty())
        {
            auto call_uri = "sip:" + call_addr + ";transport=tcp";
            auto ok = PlaceCall(sip_endpoint, call_uri);
            if (!ok)
                return -1;
        }
        io_service_.run();
    }
    catch(boost::system::system_error& e)
    {
        log.Fatal("Failed to intialize io service! Error code - %d", e.code().value());
        return -1;
    }
    catch(std::exception& e)
    {
        log.Fatal("Unknown error! %s", e.what());
        return -1;
    }

    sip_endpoint.Destroy();
    media_session_provider.Destroy();

    log.Debug("Stopped process.");
    return 0;
}
