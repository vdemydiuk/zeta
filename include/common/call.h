//
//  icall.h
//  sip
//
//  Created by Vyacheslav Demidyuk on 12/25/20.
//

#ifndef CALL_H
#define CALL_H

#include <boost/signals2.hpp>

namespace zt
{
    struct Session;

    enum class CallState
    {
        Null,
        Calling,
        Incoming,
        Connecting,
        Confirmed,
        Disconnected
    };
    
    class Call
    {
        typedef boost::signals2::signal<void (CallState)> OnCallStateChangedSignal;
    public:           
        virtual ~Call() {}
        
        virtual CallState getCallState() = 0;
        virtual Session& getSession() = 0;
        
        virtual bool Answer() = 0;
        virtual bool Hangup(const std::string& reason = "") = 0;

        OnCallStateChangedSignal OnCallStateChangedEvent;
    };
}

#endif /* CALL_H */
