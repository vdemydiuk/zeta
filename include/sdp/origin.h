//
//  origin.h
//  zt
//
//  Created by Vyacheslav Demidyuk on 10/14/20.
//

#ifndef SDP_ORIGIN_H
#define SDP_ORIGIN_H

namespace zt
{
    namespace sdp
    {
        class Origin
        {
        public:
            Origin(const std::string& user, int64_t session_id, int64_t version,
                   const std::string& net_type, const std::string& addr_type, const std::string& address)
            :user_(user)
            ,session_id_(session_id)
            ,version_(version)
            ,net_type_(net_type)
            ,addr_type_(addr_type)
            ,address_(address)
            {
            }
            
            Origin()
            :session_id_(0)
            ,version_(0)
            {
            }
            
            std::string getUser() const { return user_; }
            uint64_t getSessionId() const { return session_id_; }
            uint64_t getVersion() const { return version_; }
            std::string getNetType() const { return net_type_; }
            std::string getAddrType() const { return addr_type_; }
            std::string getAddress() const { return address_; }
            
            void setUser(const std::string& user) { user_ = user; }
            void setSessionId(uint64_t session_id) { session_id_ = session_id; }
            void setVersion(uint64_t version) { version_ = version; }
            void setNetType(const std::string& net_type) { net_type_ = net_type; }
            void setAddrType(const std::string& addr_type) { addr_type_ = addr_type; }
            void setAddress(const std::string& address) { address_ = address; }
            
            std::string toString()
            {
                return "o=" + user_ + " " + std::to_string(session_id_) + " "
                        + std::to_string(version_) + " " + net_type_ + " "
                        + addr_type_ + " " + address_ + "\r\n";
            }
            
        private:
            std::string user_;
            uint64_t    session_id_;
            uint64_t    version_;
            std::string net_type_;
            std::string addr_type_;
            std::string address_;
        };
    }
}

#endif /* SDP_ORIGIN_H */
