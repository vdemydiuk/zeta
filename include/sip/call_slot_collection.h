#ifndef CALL_COLLECTION_H
#define CALL_COLLECTION_H

#include <map>
#include <mutex>
#include "log/logger.h"

namespace zt
{
    namespace sip
    {
        class CallSlot;
        class SipCall;
        
        class CallSlotCollection
        {
        public:
            CallSlotCollection(uint32_t max_calls);
            ~CallSlotCollection();
            
            uint32_t getMaxCalls() { return max_calls_; }
            CallSlot* AcquireSlot(const std::shared_ptr<SipCall>& call);
            CallSlot* getSlot(uint32_t id);

        private:
            typedef std::map<uint32_t, std::unique_ptr<CallSlot>> SlotMap;
            
            log::Logger     log_;
            uint32_t        max_calls_;
            SlotMap         slots_;
            uint32_t        next_id_;
            std::mutex      mutex_;
        };
    }//sip
}//zt

#endif //CALL_COLLECTION_H
