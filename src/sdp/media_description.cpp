//
//  media_description.cpp
//  zt
//
//  Created by Vyacheslav Demidyuk on 10/7/20.
//

#include "sdp/media_description.h"
#include <sstream>

using namespace zt::sdp;

MediaDescription::MediaDescription(MediaType type, unsigned int port)
:media_type_(type)
,port_(port)
{
}

MediaType MediaDescription::getMediaType() const
{
    return media_type_;
}

uint32_t MediaDescription::getPort() const
{
    return port_;
}

uint32_t MediaDescription::getNumPorts() const
{
    return num_ports_;
}

std::string MediaDescription::getProto() const
{
    return proto_;
}

std::vector<std::string> MediaDescription::getFormats() const
{
    return formats_;
}

void MediaDescription::setMediaType(MediaType type)
{
    media_type_ = type;
}

void MediaDescription::setPort(uint32_t port)
{
    port_ = port;
}

void MediaDescription::setNumPorts(uint32_t num)
{
    num_ports_ = num;
}

void MediaDescription::setProto(const std::string& proto)
{
    proto_ = proto;
}

void MediaDescription::addFormat(const std::string& format)
{
    if (!format.empty())
        formats_.push_back(format);
}

void MediaDescription::addFormat(uint32_t format)
{
    addFormat(std::to_string(format));
}

std::string MediaDescription::toString() const
{
    std::stringstream value;

    value << "m=" << sdp::toString(media_type_);
    value << " " << port_;
    if (num_ports_ > 0)
        value << "/" << num_ports_;
    value << " " << proto_;
    for (std::string f : formats_)
        value << " " + f;
    value << "\r\n";
    
    // if this media session is disabled (port 0), then other information is not necessary
    if (port_ == 0)
        return value.str();
    
    value << Description::toString();

    return value.str();
}
