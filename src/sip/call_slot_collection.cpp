#include "sip/call_slot_collection.h"
#include "sip/call_slot.h"
#include "common/call.h"

using namespace zt;
using namespace zt::sip;

CallSlotCollection::CallSlotCollection(uint32_t max_calls)
:log_("pjsip::CallSlotCollection")
,max_calls_(max_calls)
,next_id_(0)
{
    log_.Trace("CallSlotCollection::ctor");
}

CallSlotCollection::~CallSlotCollection()
{
    log_.Trace("CallSlotCollection::dtor");
}

CallSlot* CallSlotCollection::AcquireSlot(const std::shared_ptr<SipCall>& call)
{
    std::lock_guard<std::mutex> lock(mutex_);
    
    CallSlot* slot = nullptr;
    
    //TODO: change algorithm to round-robin (like pjsip)
    
    for(uint32_t id = 0; id < next_id_; ++id)
    {
        auto it = slots_.find(id);
        if (it != slots_.end() && it->second->isEmpty())
        {
            slot = it->second.get();
            break;
        }
    }
    
    if (slot == nullptr
        && next_id_ < max_calls_)
    {
        slots_[next_id_] = std::make_unique<CallSlot>(next_id_);
        slot = slots_[next_id_].get();
        next_id_++;
        
        log_.Trace("%s: created new call slot with id %d", __FUNCTION__, slot->getId());
    }
    
    if (slot)
        slot->setCall(std::weak_ptr<SipCall>(call));
    
    return slot;
}

CallSlot* CallSlotCollection::getSlot(uint32_t id)
{
    std::lock_guard<std::mutex> lock(mutex_);

    auto it = slots_.find(id);
    return (it == slots_.end()) ? nullptr : it->second.get();
}
