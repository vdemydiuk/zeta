//
//  description.cpp
//  zt
//
//  Created by Vyacheslav Demidyuk on 11/5/20.
//

#include "sdp/description.h"
#include <sstream>

using namespace zt::sdp;

std::shared_ptr<ConnectionData> Description::getConnection() const
{
    return connection_;
}

std::shared_ptr<Bandwidth> Description::getBandwidth() const
{
    return bandwidth_;
}

void Description::setConnection(const std::shared_ptr<ConnectionData>& connection)
{
    connection_ = connection;
}

void Description::setBandwidth(const std::shared_ptr<Bandwidth>& bandwidth)
{
    bandwidth_ = bandwidth;
}

void Description::addAttribute(const std::shared_ptr<Attribute> &attr)
{
    if (attr)
        attributes_.push_back(attr);
}

void Description::addAttribute(const std::string& attr)
{
    addAttribute(std::make_shared<GenericAttribute>(attr));
}

void Description::addAttribute(const std::string& attr, const std::string& value)
{
    addAttribute(std::make_shared<GenericAttribute>(attr, value));
}

uint32_t Description::getAttributeCount() const
{
    return attributes_.size();
}

std::string Description::toString() const
{
    std::stringstream value;

    if (connection_)
        value << connection_->toString();
    if (bandwidth_)
        value << bandwidth_->toString();
    for (auto attr : attributes_)
        value << attr->toString();
    
    return value.str();
}
