//
//  media_description.h
//  zt
//
//  Created by Vyacheslav Demidyuk on 10/7/20.
//

#ifndef SDP_MEDIA_DESCRIPTION_H
#define SDP_MEDIA_DESCRIPTION_H

#include "description.h"
#include "media_type.h"

namespace zt
{
    namespace sdp
    {
        class MediaDescription : public Description
        {
        public:
            MediaDescription(MediaType type, uint32_t port);
            
            MediaType getMediaType() const;
            uint32_t getPort() const;
            uint32_t getNumPorts() const;
            std::string getProto() const;
            std::vector<std::string> getFormats() const;

            void setMediaType(MediaType type);
            void setPort(uint32_t port);
            void setNumPorts(uint32_t num);
            void setProto(const std::string& proto);
            
            void addFormat(const std::string& format);
            void addFormat(uint32_t format);
            
            std::string toString() const override;
            
        private:
            MediaType                       media_type_;
            uint32_t                        port_;
            uint32_t                        num_ports_{0};
            std::string                     proto_;
            std::vector<std::string>        formats_;
        };
    }
}

#endif /* SDP_MEDIA_DESCRIPTION_H */
