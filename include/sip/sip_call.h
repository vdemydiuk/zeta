#ifndef SIP_CALL_H
#define SIP_CALL_H

#include "log/logger.h"
#include "common/call.h"
#include <pjsip_ua.h>
#include <mutex>

namespace zt
{
    namespace sdp
    {
        class SessionDescription;
    }

    namespace sip
    {
        class SipCall : public Call
        {
        public:
            SipCall(pjsip_inv_session* inv, std::unique_ptr<Session> session);
            ~SipCall() override;
            
            bool isActive();
            
            CallState getCallState() override;
            Session& getSession() override { return *session_.get(); }
            
            bool Answer() override;
            bool Hangup(const std::string& reason = "") override;
            
        private:
            friend class SipEndpoint;
            bool Invite();
            void OnNegotiationCompleted(const pjmedia_sdp_session* local_sdp, const pjmedia_sdp_session* remote_sdp);
            void OnCallStateChanged();
            std::unique_ptr<sdp::SessionDescription> CreateSessionDescription();
            
            static CallState toCallState(pjsip_inv_state state);
            
            log::Logger             log_;
            pj_time_val             start_time_;
            pjsip_inv_session*      inv_{nullptr};
            std::unique_ptr<Session> session_;
            std::recursive_mutex    mutex_;
        };
    }
}

#endif //PJSIP_CALL_H
