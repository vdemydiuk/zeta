//
//  parser.cpp
//  zt
//
//  Created by Vyacheslav Demidyuk on 11/11/20.
//

#include "sdp/sdp_parser.h"
#include "sdp/session_description.h"
#include <sstream>

using namespace zt::sdp;

std::map<std::string,MediaType> SdpParser::media_types_ =
{
    { "audio", MediaType::AUDIO },
    { "video", MediaType::VIDEO },
    { "text", MediaType::TEXT },
    { "application", MediaType::APPLICATION },
    { "message", MediaType::MESSAGE }
};

std::map<std::string,DirectionType> SdpParser::direction_types_ =
{
    { "sendrecv", DirectionType::SENDRECV },
    { "sendonly", DirectionType::SENDONLY },
    { "recvonly", DirectionType::RECVONLY },
    { "inactive", DirectionType::INACTIVE }
};

std::map<std::string,BandwidthType> SdpParser::bandwidth_types_ =
{
    { "CT", BandwidthType::CT },
    { "AS", BandwidthType::AS },
    { "TIAS", BandwidthType::TIAS },
    { "RR", BandwidthType::RR },
    { "RS", BandwidthType::RS},
};

std::map<char,std::vector<ParserRule>> SdpParser::rules_ =
{
    {
       //version
        'v',
        {
            {
                std::regex("^(\\d*)$"),
                [](const std::smatch& m, Description* d)
                {
                    auto s = static_cast<SessionDescription*>(d);
                    if (!m.str(0).empty())
                        s->setVersion(SdpParser::toInt32(m.str(0)));
                }
            }
        }
    },
    {
        //session name
        's',
        {
            {
                std::regex("(.*)"),
                [](const std::smatch& m, Description* d)
                {
                    auto s = static_cast<SessionDescription*>(d);
                    if (!m.str(0).empty())
                        s->setSessionName(m.str(0));
                }
            }
        }
    },

    {
        //uri
        'u',
        {
            {
                std::regex("(.*)"),
                [](const std::smatch& m, Description* d)
                {
                    auto s = static_cast<SessionDescription*>(d);
                    if (!m.str(0).empty())
                        s->setUri(m.str(0));
                }
            }
        }
    },
    {
        //email
        'e',
        {
            {
                std::regex("(.*)"),
                [](const std::smatch& m, Description* d)
                {
                    auto s = static_cast<SessionDescription*>(d);
                    if (!m.str(0).empty())
                        s->setEmail(m.str(0));
                }
            }
        }
    },
    {
        //origin
        'o',
        {
            {
                std::regex("^(\\S*) (\\d*) (\\d*) (\\S*) (IP\\d) (\\S*)"),
                [](const std::smatch& m, Description* d)
                {
                    auto s = static_cast<SessionDescription*>(d);
                    auto origin = std::make_shared<Origin>();
                    if (!m.str(1).empty())
                        origin->setUser(m.str(1));
                    if (!m.str(2).empty())
                        origin->setSessionId(SdpParser::toInt64(m.str(2)));
                    if (!m.str(3).empty())
                        origin->setVersion(SdpParser::toInt64(m.str(3)));
                    if (!m.str(4).empty())
                        origin->setNetType(m.str(4));
                    if (!m.str(5).empty())
                        origin->setAddrType(m.str(5));
                    if (!m.str(6).empty())
                        origin->setAddress(m.str(6));
                    s->setOrigin(origin);
                }
            }
        }
    },
    {
        //information
        'i',
        {
            {
                std::regex("(.*)"),
                [](const std::smatch& m, Description* d)
                {
                    auto s = static_cast<SessionDescription*>(d);
                    if (!m.str(0).empty())
                        s->setInformation(m.str(0));
                }
            }
        }
    },
    {
        //timing
        't',
        {
            {
                std::regex("^(\\d*) (\\d*)"),
                [](const std::smatch& m, Description* d)
                {
                    auto s = static_cast<SessionDescription*>(d);
                    auto start = m.str(1).empty() ? 0 : SdpParser::toInt32( m.str(1));
                    auto stop = m.str(2).empty() ? 0 : SdpParser::toInt32(m.str(2));
                    s->setTiming(std::make_shared<Timing>(start, stop));
                }
            }
        }
    },
    {
        //connection
        'c',
        {
            {
                std::regex("^(\\S*) IP(\\d) ([^\\\\S/]*)(?:/(\\d*))?(?:/(\\d*))?"),
                [](const std::smatch& m, Description* d)
                {
                    auto net_type = m.str(1);
                    auto addr_type = !m.str(2).empty() ? "IP" + m.str(2) : "";
                    auto addr = m.str(3);
                    auto ttl = !m.str(4).empty() ? SdpParser::toInt32(m.str(4)) : 0;
                    auto connection = std::make_shared<ConnectionData>(net_type, addr_type, addr, ttl);
                    if (!m.str(5).empty())
                        connection->setNumAddr(SdpParser::toInt32(m.str(5)));
                    d->setConnection(connection);
                }
            }
        }
    },
    {
        //bandwidth
        'b',
        {
            {
                std::regex("^(TIAS|AS|CT|RR|RS):(\\d*)"),
                [](const std::smatch& m, Description* d)
                {
                    if (!m.str(1).empty() && !m.str(2).empty())
                    {
                        auto bandwidth_type = SdpParser::toBandwidthType(m.str(1));
                        auto bandwidth = SdpParser::toInt32(m.str(2));
                        d->setBandwidth(std::make_shared<Bandwidth>(bandwidth_type, bandwidth));
                    }
                }
            }
        }
    },
    {
        //media
        'm',
        {
            {
                std::regex("^(\\w*) (\\d*)(?:/(\\d*))? ([\\w\\/]*)(?: (.*))?"),
                [](const std::smatch& m, Description* d)
                {
                    auto media = static_cast<MediaDescription*>(d);
                    if (!m.str(1).empty())
                        media->setMediaType(SdpParser::toMediaType(m.str(1)));
                    if (!m.str(2).empty())
                        media->setPort(SdpParser::toInt32(m.str(2)));
                    if (!m.str(3).empty())
                        media->setNumPorts(SdpParser::toInt32(m.str(3)));
                    if (!m.str(4).empty())
                        media->setProto(m.str(4));
                    if (!m.str(5).empty())
                    {
                        std::istringstream f(m.str(5));
                        std::string s;
                        while (getline(f, s, ' ')) {
                            media->addFormat(s);
                        }
                    }
                }
            }
        }
    },
    {
        //attribute
        'a',
        {
            //a=rtpmap
            {
                std::regex("^rtpmap:(\\d*) ([\\w\\-\\.]*)(?:\\s*\\/(\\d*)(?:\\s*\\/(\\S*))?)?"),
                [](const std::smatch& m, Description* d)
                {
                    auto ptype = !m.str(1).empty() ? SdpParser::toInt32(m.str(1)) : 128;
                    auto encoder_name = m.str(2);
                    auto clock_rate = !m.str(3).empty() ? SdpParser::toInt32(m.str(3)) : 0;
                    auto parameters = m.str(4);
                    d->addAttribute(std::make_shared<RtpMapAttr>(ptype, encoder_name, clock_rate, parameters));
                }
            },
            //a=fmtp
            {
                std::regex("^fmtp:(\\d*) (.*)"),
                [](const std::smatch& m, Description* d)
                {
                    auto ptype = !m.str(1).empty() ? SdpParser::toInt32(m.str(1)) : 128;
                    auto parameters = m.str(2);
                    d->addAttribute(std::make_shared<FormatAttr>(ptype, parameters));
                }
            },
            //direction (sendrecv|sendonly|recvonly|inactive)
            {
                std::regex("^(sendrecv|recvonly|sendonly|inactive)"),
                [](const std::smatch& m, Description* d)
                {
                    d->addAttribute(std::make_shared<DirectionAttr>(SdpParser::toDirectionType(m.str(1))));
                }
            },
            //crypto
            {
                std::regex("^crypto:(\\d*) ([\\w_]*) (.*)"),
                [](const std::smatch& m, Description* d)
                {
                    std::regex reg_crypto_key("^(\\w*):(\\w*)(?:\\|(2\\^\\d+|\\d+|\\d+:\\d+))?(?:\\|(\\d+:\\d+))?(?:;)?");
                    std::regex reg_crypto_lifetime("^(?:2\\^(\\d+)|(\\d+))$");
                    std::regex reg_crypto_mki("^(\\d+):(\\d+)$");
                    std::regex reg_crypto_fec("^FEC_ORDER=(\\w+)$");
                    std::regex reg_crypto_kdr_or_wsh("^(KDR|WSH)=(\\d+)$");
                    std::regex reg_crypto_srtp("^(UNENCRYPTED_SRTP|UNENCRYPTED_SRTCP|UNAUTHENTICATED_SRTP)$");
                    
                    if (!m.str(1).empty())
                    {
                        auto tag = SdpParser::toInt32(m.str(1));
                        auto crypto_attr = std::make_shared<CryptoAttr>(tag, m.str(2));
                        if (!m.str(2).empty())
                        {
                            std::istringstream f(m.str(3));
                            std::string s;
                            while (getline(f, s, ' '))
                            {
                                if (std::regex_search(s, reg_crypto_key))
                                {
                                    std::smatch match_key;
                                    std::regex_search(s, match_key, reg_crypto_key);
                                    
                                    auto crypto_key = std::make_shared<CryptoAttr::Key>(match_key.str(1), match_key.str(2));
                                    if (!match_key.str(3).empty())
                                    {
                                        std::string cur_str = match_key.str(3);
                                        if (std::regex_search(cur_str, reg_crypto_lifetime))
                                        {
                                            std::smatch match_lifetime;
                                            std::regex_search(cur_str, match_lifetime, reg_crypto_lifetime);
                                            
                                            std::shared_ptr<CryptoAttr::Lifetime> lifetime;
                                            if (!match_lifetime.str(1).empty())
                                                lifetime = std::make_shared<CryptoAttr::Lifetime>(SdpParser::toInt32(match_lifetime.str(1)));
                                            else if (!match_lifetime.str(2).empty())
                                                lifetime = std::make_shared<CryptoAttr::Lifetime>(SdpParser::toInt32(match_lifetime.str(2)), false);
                                            crypto_key->setLifetime(lifetime);
                                            
                                            cur_str = match_key.str(4);
                                        }
                                        if (std::regex_search(cur_str, reg_crypto_mki))
                                        {
                                            std::smatch match_mki;
                                            std::regex_search(cur_str, match_mki, reg_crypto_mki);
                                            
                                            auto value = SdpParser::toInt32(match_mki.str(1));
                                            auto length = SdpParser::toInt32(match_mki.str(2));
                                            crypto_key->setMKI(std::make_shared<CryptoAttr::MKI>(value, length));
                                        }
                                    }
                                    crypto_attr->addKey(crypto_key);
                                }
                                else if(std::regex_search(s, reg_crypto_fec))
                                {
                                    std::smatch match_key;
                                    std::regex_search(s, match_key, reg_crypto_fec);
                                    
                                    auto fec_order = match_key.str(1) == "SRTP_FEC" ? FecOrderType::SRTP_FEC : FecOrderType::FEC_SRTP;
                                    crypto_attr->addParam(std::make_shared<CryptoAttr::FecOrderParam>(fec_order));
                                }
                                else if(std::regex_search(s, reg_crypto_kdr_or_wsh))
                                {
                                    std::smatch match_key;
                                    std::regex_search(s, match_key, reg_crypto_kdr_or_wsh);
                                    
                                    auto value = SdpParser::toInt32(match_key.str(2));
                                    crypto_attr->addParam(std::make_shared<CryptoAttr::IntParam>(match_key.str(1), value));
                                }
                                else if(std::regex_search(s, reg_crypto_srtp))
                                {
                                    std::smatch match_key;
                                    std::regex_search(s, match_key, reg_crypto_srtp);
                                    
                                    crypto_attr->addParam(std::make_shared<CryptoAttr::SrtpParam>(match_key.str(1)));
                                }
                            }
                        }
                        d->addAttribute(crypto_attr);
                    }
                }
            },
            //generic attribute
            {
                std::regex("^([\\w-]*)(?::(.*))?"),
                [](const std::smatch& m, Description* d)
                {
                    d->addAttribute(m.str(1), m.str(2));
                }
            }
        }
    }
};

void SdpParser::Parse(const std::string& sdp, SessionDescription& session_description)
{
    static const std::regex ValidLineRegex("^([a-z])=(.*)");
    std::stringstream sdp_stream(sdp);
    std::string sdp_line;
    
    Description* cur_desc = &session_description;
    
    while (std::getline(sdp_stream, sdp_line, '\n'))
    {
        // Remove \r if lines are separated with \r\n (as mandated in SDP).
        if (sdp_line.size() && sdp_line[sdp_line.length() - 1] == '\r')
            sdp_line.pop_back();
        
        // Ensure it's a valid SDP line.
        if (!std::regex_search(sdp_line, ValidLineRegex))
            continue;
        
        char type = sdp_line[0];
        std::string content = sdp_line.substr(2);
        
        if (type == 'm')
        {
            auto media_description = std::make_shared<MediaDescription>(MediaType::UNKNOWN, 0);
            session_description.addMediaDescription(media_description);
            cur_desc = media_description.get();
        }
        
        if (rules_.find(type) != rules_.end())
        {
            auto& rule_vec = rules_[type];
            for(auto& rule : rule_vec)
            {
                if (std::regex_search(content, rule.reg))
                {
                    std::smatch match;
                    std::regex_search(content, match, rule.reg);
                    rule.func_apply(match, cur_desc);
                    break;
                }
            }
        }
    }
}

uint32_t SdpParser::toInt32(const std::string& str)
{
    std::istringstream iss(str);
    int l;
    
    iss >> std::noskipws >> l;
    
    if (iss.eof() && !iss.fail())
        return std::stoi(str);
    else
        return 0;
}

uint64_t SdpParser::toInt64(const std::string& str)
{
    std::istringstream iss(str);
    long long ll;
    
    iss >> std::noskipws >> ll;
    
    if (iss.eof() && !iss.fail())
        return std::stoll(str);
    else
        return 0;
}

MediaType SdpParser::toMediaType(const std::string& str)
{
    if (media_types_.find(str) != media_types_.end())
        return media_types_[str];
    return MediaType::UNKNOWN;
}

DirectionType SdpParser::toDirectionType(const std::string& str)
{
    if (direction_types_.find(str) == direction_types_.end())
        throw ;
    return direction_types_[str];
}

BandwidthType SdpParser::toBandwidthType(const std::string& str)
{
    if (bandwidth_types_.find(str) == bandwidth_types_.end())
        throw ;
    return bandwidth_types_[str];
}
