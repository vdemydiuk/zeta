//
//  timing.h
//  zt
//
//  Created by Vyacheslav Demidyuk on 10/20/20.
//

#ifndef SDP_TIME_H
#define SDP_TIME_H

#include <stdint.h>

namespace zt
{
    namespace sdp
    {
        class Timing
        {
        public:
            Timing(uint32_t start, uint32_t stop)
            :start_(start)
            ,stop_(stop)
            {}

            uint32_t getStart() const { return start_; }
            uint32_t getStop() const { return stop_; }
            
            std::string toString() const { return "t=" + std::to_string(start_) + " " + std::to_string(stop_) + "\r\n"; }

        private:
            uint32_t start_;
            uint32_t stop_;
        };
    }
}

#endif /* SDP_TIME_H */
