//
//  console_controller.h
//  meeting
//
//  Created by Vyacheslav Demidyuk on 5/21/20.
//

#ifndef CONSOLE_CONTROLLER_H
#define CONSOLE_CONTROLLER_H

#include <boost/asio.hpp>
#include <iostream>
#include "log/logger.h"

namespace zt
{
    class ConsoleController
    {
    public:
        class Callback
        {
        public:
            virtual ~Callback() {}
            virtual void OnRun() = 0;
            virtual void OnQuit() = 0;
        };
        
        ConsoleController(boost::asio::io_service& service, Callback& callback);
        ~ConsoleController();
        
        void Run();

    private:
        void ReadInput();
        void FinishReadInput(const boost::system::error_code& ec, std::size_t len);
        void WriteOutput(const char* message);
        void FunishWriteOutput(const boost::system::error_code& ec, std::size_t len);

        log::Logger log_;
        boost::asio::io_service& service_;
        boost::asio::posix::stream_descriptor stdin_;
        boost::asio::posix::stream_descriptor stdout_;
        boost::asio::streambuf input_buf_;
        boost::asio::streambuf output_buf_;
        std::istream input_;
        std::ostream output_;
        Callback& callback_;
    };
}

#endif // CONSOLE_CONTROLLER_H
