#ifndef CONFIG_H
#define CONFIG_H

#define DEFAULT_WORKING_THREAD_COUNT 1

namespace zt
{
    namespace sip
    {
        struct Config
        {
            int Port{0};
            std::string LocalAddress;
            bool SipUDP{false};
            uint32_t WorkingThreadCount{DEFAULT_WORKING_THREAD_COUNT};
            bool UseIPv6{false};
        };
    }
}

#endif //CONFIG_H
