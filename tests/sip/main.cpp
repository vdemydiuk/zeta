#include "gtest/gtest.h"
#include "log/configurator.h"
#include "sip/call_slot.h"
#include "sip/call_slot_collection.h"
#include "sip/sip_call.h"
#include "sip/sip_endpoint.h"
#include "common/session.h"
#include "common/session_provider.h"
#include "sdp/sdp_parser.h"
#include <chrono>
#include <thread>
//#include <vector>
#include <queue>

using namespace zt;
using namespace zt::sip;
using namespace std::chrono_literals;
using namespace std::chrono;

#define LOCAL_PORT 5065
#define LOCAL_ADDRESS "127.0.0.1"

namespace
{
    const std::string SDP_AUDIO_VIDEO_1 =
                                "v=0\r\n"
                                "o=- 1 0 IN IP4 127.0.0.1\r\n"
                                "s=pjsip1\r\n"
                                "t=0 0\r\n"
                                "c=IN IP4 127.0.0.1\r\n"
                                "m=audio 49232 RTP/AVP 0 8 110\r\n"
                                "a=rtpmap:8 PCMA/8000\r\n"
                                "a=rtpmap:110 opus/48000/2\r\n"
                                "a=sendrecv\r\n"
                                "m=video 62358 RTP/AVP 99 98\r\n"
                                "a=rtpmap:99 H264/90000\r\n"
                                "a=rtpmap:98 h263-1998/90000\r\n"
                                "a=sendrecv\r\n"
                                "m=application 63360 RTP/AVP 99\r\n"
                                "a=sendrecv\r\n"
                                "a=rtpmap:99 h224/4800\r\n";
    
    const std::string SDP_AUDIO_VIDEO_2 =
                                "v=0\r\n"
                                "o=- 2 0 IN IP4 127.0.0.1\r\n"
                                "s=pjsip2\r\n"
                                "t=0 0\r\n"
                                "c=IN IP4 127.0.0.1\r\n"
                                "m=audio 58560 RTP/AVP 8 0\r\n"
                                "a=rtpmap:8 PCMA/8000\r\n"
                                "a=sendrecv\r\n"
                                "m=video 62400 RTP/AVP 97 98\r\n"
                                "a=rtpmap:97 H264/90000\r\n"
                                "a=rtpmap:98 h263-1998/90000\r\n"
                                "a=recvonly\r\n";

    const std::string SDP_AUDIO_ONLY =
                                "v=0\r\n"
                                "o=- 1 0 IN IP4 127.0.0.1\r\n"
                                "s=pjsip1\r\n"
                                "t=0 0\r\n"
                                "c=IN IP4 127.0.0.1\r\n"
                                "m=audio 49232 RTP/AVP 0 8 110\r\n"
                                "a=rtpmap:8 PCMA/8000\r\n"
                                "a=rtpmap:110 opus/48000/2\r\n"
                                "a=sendrecv\r\n";

    const std::string SDP_VIDEO_ONLY =
                                "v=0\r\n"
                                "o=- 1 0 IN IP4 127.0.0.1\r\n"
                                "s=pjsip1\r\n"
                                "t=0 0\r\n"
                                "c=IN IP4 127.0.0.1\r\n"
                                "m=audio 49232 RTP/AVP 0 8 110\r\n"
                                "a=rtpmap:8 PCMA/8000\r\n"
                                "a=rtpmap:110 opus/48000/2\r\n"
                                "a=sendrecv\r\n";

/****************************************************************
 SipCallTest
 ****************************************************************/
    class SipCallSlotTest: public ::testing::Test
    {
    public:
        const uint32_t SLOT_ID = 16;

    protected:
        SipCallSlotTest()
        {
        }
        
        ~SipCallSlotTest() override
        {
        }
        
        void SetUp() override
        {
            call_slot_ = std::make_unique<CallSlot>(SLOT_ID);
        }
        
        void TearDown() override
        {
            call_slot_.release();
        }

        std::unique_ptr<CallSlot>  call_slot_;
    };
    
    TEST_F(SipCallSlotTest, GetId)
    {
        EXPECT_EQ(SLOT_ID, call_slot_->getId());
    }
    
    TEST_F(SipCallSlotTest, SetCall)
    {
        {
            auto call = std::make_shared<SipCall>(nullptr, nullptr);
            call_slot_->setCall(std::weak_ptr<SipCall>(call));
            
            EXPECT_NE(call_slot_->getCall(), nullptr);
        }
        
        // There is no strong reference to call, weak reference in slot does not contain the call
        EXPECT_EQ(call_slot_->getCall(), nullptr);
    }
    
/****************************************************************
 SipCallCollectionTest
 ****************************************************************/
    
    class SipCallSlotCollectionTest: public ::testing::Test
    {
    protected:
        static const int MAX_CALLS_NUM;
        
        SipCallSlotCollectionTest()
        {
        }

        ~SipCallSlotCollectionTest() override
        {
        }

        void SetUp() override
        {
            slots_ = std::make_unique<CallSlotCollection>(MAX_CALLS_NUM);
        }

        void TearDown() override
        {
            slots_.release();
        }

        std::unique_ptr<CallSlotCollection> slots_;
    };
    
    const int SipCallSlotCollectionTest::MAX_CALLS_NUM = 1000;

    TEST_F(SipCallSlotCollectionTest, AcquireSlot)
    {
        auto slot = slots_->AcquireSlot(std::make_shared<SipCall>(nullptr, nullptr));
        ASSERT_NE(slot, nullptr);
        auto id = slot->getId();
        EXPECT_EQ(0, id);
        auto call_by_id = slots_->getSlot(id);
        EXPECT_EQ(slot, call_by_id);
    }

    TEST_F(SipCallSlotCollectionTest, AcquireSlot_100)
    {
        for(int i = 0; i < 100; i++)
        {
            auto slot = slots_->AcquireSlot(std::make_shared<SipCall>(nullptr, nullptr));
            ASSERT_NE(slot, nullptr);
            auto id = slot->getId();
            EXPECT_EQ(0, id);
        }

        auto call_id_0 = slots_->getSlot(0);
        EXPECT_NE(call_id_0, nullptr);

        for(int i = 1; i < 100; i++)
        {
            auto call_id_i = slots_->getSlot(i);
            EXPECT_EQ(call_id_i, nullptr);
        }
    }

    TEST_F(SipCallSlotCollectionTest, AcquireCall_100_Keep_and_Clear)
    {
        std::vector<std::shared_ptr<Call>> calls_;

        for(int i = 0; i < 100; i++)
        {
            auto call = std::make_shared<SipCall>(nullptr, nullptr);
            auto slot = slots_->AcquireSlot(call);
            ASSERT_NE(slot, nullptr);

            calls_.push_back(call);
            slot->setCall(call);
        }

        for(int i = 0; i < 100; i++)
        {
            auto slot = slots_->getSlot(i);
            EXPECT_FALSE(slot->isEmpty());
            auto call = slot->getCall();
            EXPECT_NE(call, nullptr);
        }

        calls_.clear();

        for(int i = 0; i < 100; i++)
        {
            auto slot = slots_->getSlot(i);
            EXPECT_TRUE(slot->isEmpty());
            auto call = slot->getCall();
            EXPECT_EQ(call, nullptr);
        }
    }

    TEST_F(SipCallSlotCollectionTest, AcquireCall_MaxNum)
    {
        std::vector<std::shared_ptr<Call>> calls_;

        for(int i = 0; i < MAX_CALLS_NUM; i++)
        {
            auto call = std::make_shared<SipCall>(nullptr, nullptr);
            auto slot = slots_->AcquireSlot(call);
            ASSERT_NE(slot, nullptr);

            calls_.push_back(call);
            slot->setCall(call);
        }

        //calls collection does not acquire more than MAX_CALLS_NUM
        auto slot_max_num = slots_->AcquireSlot(std::make_shared<SipCall>(nullptr, nullptr));
        ASSERT_EQ(slot_max_num, nullptr);
    }

/****************************************************************
 SipEndpointTest
 ****************************************************************/

    class SessionStub : public Session
    {
    public:
        SessionStub(const std::string& sdp)
        :sdp_(sdp)
        {
        }

        void CreateSessionDescription(CreateSessionCallback callback) override
        {
            auto description = std::make_unique<sdp::SessionDescription>();
            sdp::SdpParser::Parse(sdp_, *description.get());
            callback(std::move(description));
        }

        bool Update(const sdp::SessionDescription& remote_sdp) override
        {
            updated_ = true;
            return true;
        }

        void Close() {}

        bool isUpdated() { return updated_; }

    private:
        std::string sdp_;
        bool updated_{false};
    };

    class SessionProviderStub : public SessionProvider
    {
    public:
        void PushSdp(const std::string& sdp) 
        {
            sdp_queue_.push(sdp);
        }

    protected:
        std::unique_ptr<Session> CreateSession() override
        {
            if (sdp_queue_.size() == 0)
                return nullptr;

            auto session = std::make_unique<SessionStub>(sdp_queue_.front());
            sdp_queue_.pop();
            return session;
        }

    private:
        std::queue<std::string> sdp_queue_;
    };

    class SipEndpointTest: public ::testing::Test
    {
    protected:
        SipEndpointTest()
        :endpoint_(session_provider_)
        {
        }

        ~SipEndpointTest() override
        {
        }

        std::shared_ptr<Call> getIncomingCall()
        {
            std::lock_guard<std::mutex> lock(mutex_);
            return incoming_call_;
        }

        void SetUp() override
        {
            Config sip_config{LOCAL_PORT, LOCAL_ADDRESS};
            auto created = endpoint_.Create(sip_config);
            endpoint_.OnIncomingCallEvent.connect(
                [this](const std::shared_ptr<Call>& call)
                {
                    std::lock_guard<std::mutex> lock(mutex_);
                    incoming_call_ = call;
                    return true;
                }
            );

            uri_to_call_ = "sip:" + std::string(LOCAL_ADDRESS) + ":" + std::to_string(LOCAL_PORT) + std::string(";transport=tcp");
        }

        void TearDown() override
        {
            endpoint_.Destroy();
        }

    protected:
        SessionProviderStub session_provider_;
        SipEndpoint endpoint_;
        std::string uri_to_call_;

    private:
        std::shared_ptr<Call> incoming_call_{nullptr};
        std::mutex  mutex_;
    };

    TEST(SipEndpointCreateDestroyTest, CreateDestroy)
    {
        SessionProviderStub session_provider;
        SipEndpoint endpoint(session_provider);
        Config sip_config;
        
        auto created = endpoint.Create(sip_config);
        ASSERT_TRUE(created);
        ASSERT_TRUE(endpoint.isCreated());

        created = endpoint.Create(sip_config);
        ASSERT_FALSE(created);
        ASSERT_TRUE(endpoint.isCreated());

        endpoint.Destroy();
        ASSERT_FALSE(endpoint.isCreated());

        created = endpoint.Create(sip_config);
        ASSERT_TRUE(created);
        ASSERT_TRUE(endpoint.isCreated());

        endpoint.Destroy();
        ASSERT_FALSE(endpoint.isCreated());
        auto local_address = endpoint.getLocalAddress();
        ASSERT_EQ(local_address, "");
    }

    TEST_F(SipEndpointTest, GetLocalAddress)
    {
        auto local_address = endpoint_.getLocalAddress();
        ASSERT_EQ(local_address, "127.0.0.1");
        auto port = endpoint_.getLocalPort();
        ASSERT_EQ(port, 5065);
    }

    TEST_F(SipEndpointTest, MakeCallWithInvalidUri)
    {
        auto call = endpoint_.MakeCall("");
        ASSERT_EQ(call, nullptr);
    }

    TEST_F(SipEndpointTest, MakeCallWithInvalidSession)
    {
        auto call = endpoint_.MakeCall(uri_to_call_);
        ASSERT_EQ(call, nullptr);
    }

    TEST_F(SipEndpointTest, MakeCall)
    {
        session_provider_.PushSdp(SDP_AUDIO_VIDEO_1);

        auto call = endpoint_.MakeCall(uri_to_call_);
        ASSERT_NE(call, nullptr);
        ASSERT_EQ(call->getCallState(), CallState::Calling);
    }

    TEST_F(SipEndpointTest, MakeCall_Invite_IncomingCall)
    {
        session_provider_.PushSdp(SDP_AUDIO_VIDEO_1);
        session_provider_.PushSdp(SDP_AUDIO_VIDEO_2);

        auto call = endpoint_.MakeCall(uri_to_call_);
        ASSERT_NE(call, nullptr);

        auto start_time = high_resolution_clock::now();
        while(getIncomingCall() == nullptr
              && duration_cast<seconds>(high_resolution_clock::now() - start_time).count() < 5)
        {
            std::this_thread::sleep_for(10ms);
        };

        auto incoming_call = getIncomingCall();
        ASSERT_NE(incoming_call, nullptr);

        auto incoming_call_state = incoming_call->getCallState();
        ASSERT_EQ(incoming_call_state, CallState::Incoming);
    }

    TEST_F(SipEndpointTest, MakeCall_Invite_IncomingCallWithInvalidSession)
    {
        session_provider_.PushSdp(SDP_AUDIO_VIDEO_1);

        auto call = endpoint_.MakeCall(uri_to_call_);
        ASSERT_NE(call, nullptr);

        auto start_time = high_resolution_clock::now();
        while(call->getCallState() != CallState::Disconnected
              && duration_cast<seconds>(high_resolution_clock::now() - start_time).count() < 5)
        {
            std::this_thread::sleep_for(10ms);
        };

        auto call_state = call->getCallState();

        ASSERT_EQ(call_state, CallState::Disconnected);
        ASSERT_EQ(call->getCallState(), CallState::Disconnected);
    }

    TEST_F(SipEndpointTest, MakeCall_Invite_Decline)
    {
        session_provider_.PushSdp(SDP_AUDIO_VIDEO_1);
        session_provider_.PushSdp(SDP_AUDIO_VIDEO_2);

        std::atomic<CallState> call_state;

        auto outgoing_call = endpoint_.MakeCall(uri_to_call_);
        ASSERT_NE(outgoing_call, nullptr);
        outgoing_call->OnCallStateChangedEvent.connect(
            [&call_state](CallState state)
            {
                call_state = state; 
            }
        );

        auto start_time = high_resolution_clock::now();
        while(getIncomingCall() == nullptr
              && duration_cast<seconds>(high_resolution_clock::now() - start_time).count() < 5)
        {
            std::this_thread::sleep_for(10ms);
        };

        auto incoming_call = getIncomingCall();
        ASSERT_NE(incoming_call, nullptr);

        std::string reason_msg("Decline normal");
        bool hangup_result = incoming_call->Hangup(reason_msg);
        ASSERT_TRUE(hangup_result);

        start_time = high_resolution_clock::now();
        while(call_state != CallState::Disconnected
              && duration_cast<seconds>(high_resolution_clock::now() - start_time).count() < 5)
        {
            std::this_thread::sleep_for(10ms);
        };

        ASSERT_EQ(call_state, CallState::Disconnected);
        ASSERT_EQ(outgoing_call->getCallState(), CallState::Disconnected);
        ASSERT_EQ(incoming_call->getCallState(), CallState::Disconnected);
    }

    TEST_F(SipEndpointTest, MakeCall_Invite_Answer)
    {
        session_provider_.PushSdp(SDP_AUDIO_VIDEO_1);
        session_provider_.PushSdp(SDP_AUDIO_VIDEO_2);

        std::atomic<CallState> out_call_state;
        std::atomic<CallState> in_call_state;

        auto outgoing_call = endpoint_.MakeCall(uri_to_call_);
        ASSERT_NE(outgoing_call, nullptr);
        outgoing_call->OnCallStateChangedEvent.connect(
            [&out_call_state](CallState state)
            {
                out_call_state = state; 
            }
        );

        auto start_time = high_resolution_clock::now();
        while(getIncomingCall() == nullptr
              && duration_cast<seconds>(high_resolution_clock::now() - start_time).count() < 5)
        {
            std::this_thread::sleep_for(10ms);
        };

        auto incoming_call = getIncomingCall();
        ASSERT_NE(incoming_call, nullptr);

        incoming_call->OnCallStateChangedEvent.connect(
            [&in_call_state](CallState state)
            {
                in_call_state = state; 
            }
        );
        
        bool answer_result = incoming_call->Answer();
        ASSERT_TRUE(answer_result);

        start_time = high_resolution_clock::now();
        while(out_call_state != CallState::Confirmed
              && in_call_state != CallState::Confirmed
              && duration_cast<seconds>(high_resolution_clock::now() - start_time).count() < 5)
        {
            std::this_thread::sleep_for(10ms);
        };

        ASSERT_EQ(out_call_state, CallState::Confirmed);
        ASSERT_EQ(in_call_state, CallState::Confirmed);
        ASSERT_EQ(outgoing_call->getCallState(), CallState::Confirmed);
        ASSERT_EQ(incoming_call->getCallState(), CallState::Confirmed);

        auto in_call_session = dynamic_cast<SessionStub*>(&incoming_call->getSession());
        ASSERT_NE(in_call_session, nullptr);
        EXPECT_TRUE(in_call_session->isUpdated());

        auto out_call_session = dynamic_cast<SessionStub*>(&outgoing_call->getSession());
        ASSERT_NE(out_call_session, nullptr);
        EXPECT_TRUE(out_call_session->isUpdated());
    }
}//namespace

int main()
{
    log::Configurator::Setup(log::LogLevel::TRACE, log::OutputType::CONSOLE);
    ::testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}
