//
//  attribute.cpp
//  zt
//
//  Created by Vyacheslav Demidyuk on 10/12/20.
//

#include "sdp/attribute.h"
#include <sstream>

using namespace zt::sdp;

static const std::string EOL = "\r\n";

std::string Attribute::toString()
{
    std::string value = getValue();
    return "a=" + getAttribute() + (value.empty() ? "" : ":" + value) + EOL;
}

GenericAttribute::GenericAttribute(const std::string attr, const std::string& value /*= ""*/)
:attribute_(attr)
,value_(value)
{
}

RtpMapAttr::RtpMapAttr(uint32_t ptype, const std::string& encoder_name, uint32_t clock_rate, const std::string& parameters /*= ""*/)
:ptype_(ptype)
,encoder_name_(encoder_name)
,clock_rate_(clock_rate)
,enc_params_(parameters)
{
}

std::string RtpMapAttr::getAttribute() const
{
    return "rtpmap";
}

std::string RtpMapAttr::getValue() const
{
    return std::to_string(ptype_) + " " + encoder_name_ + "/" + std::to_string(clock_rate_) + (enc_params_.empty() ? "" : "/" + enc_params_);
}

DirectionAttr::DirectionAttr(DirectionType direction)
:direction_(direction)
{
}

std::string DirectionAttr::getAttribute() const
{
    switch (direction_) {
        case DirectionType::INACTIVE:
            return "inactive";
        case DirectionType::RECVONLY:
            return "recvonly";
        case DirectionType::SENDONLY:
            return "sendonly";
        case DirectionType::SENDRECV:
            return "sendrecv";
        default:
            throw std::exception();
    }
}

std::string DirectionAttr::getValue() const
{
    return "";
}

FormatAttr::FormatAttr(uint32_t ptype, const std::string& params)
:ptype_(ptype)
,params_(params)
{
}

std::string FormatAttr::getAttribute() const
{
    return "fmtp";
}

std::string FormatAttr::getValue() const
{
    return std::to_string(ptype_) + " " + params_;
}

std::string zt::sdp::to_string(FecOrderType type)
{
    switch(type)
    {
        case FecOrderType::FEC_SRTP: return "FEC_SRTP";
        case FecOrderType::SRTP_FEC: return "SRTP_FEC";
        default:
            throw;
    }
}

CryptoAttr::CryptoAttr(uint32_t tag, const std::string& crypto_suite)
:tag_(tag)
,crypto_suite_(crypto_suite)
{
}

std::string CryptoAttr::getAttribute() const
{
    return "crypto";
}

std::string CryptoAttr::getValue() const
{
    std::stringstream value;
    value << std::to_string(tag_) << " " << crypto_suite_;

    for(int i = 0; i < keys_.size(); i++)
    {
        value << " " << keys_[i]->toString();
        if (i != keys_.size() - 1)
            value << ";";
    }
    
    for(auto& param : params_)
        value << " " << param->toString();

    return value.str();
}

std::shared_ptr<CryptoAttr::Key> CryptoAttr::getKey(uint32_t index) const
{
    return index < keys_.size() ? keys_[index] : nullptr;
}

void CryptoAttr::addKey(const std::shared_ptr<Key>& key)
{
    if (key)
        keys_.push_back(key);
}

std::shared_ptr<CryptoAttr::SrtpParam> CryptoAttr::getParam(uint32_t index) const
{
    return index < params_.size() ? params_[index] : nullptr;
}

void CryptoAttr::addParam(const std::shared_ptr<CryptoAttr::SrtpParam>& param)
{
    if (param)
        params_.push_back(param);
}

std::string CryptoAttr::Key::toString()
{
    std::stringstream value;
    value << method_ << ":" << salt_;
    if (lifetime_)
    {
        value << "|";
        if (lifetime_->isExponent())
            value << "2^";
        value << lifetime_->getValue();
    }
    if (mki_)
    {
        value << "|" << mki_->getValue() << ":" << mki_->getLength();
    }
    return value.str();
}
