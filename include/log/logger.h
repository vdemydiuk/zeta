#ifndef LOG_LOGGER_H
#define LOG_LOGGER_H

#include <string>
#include <memory>

namespace zt
{
    namespace log
    {
       class Logger
        {
        public:
            Logger(const char* class_name);
            ~Logger();

            void Fatal(const char* fmt, ...);
            void Error(const char* fmt, ...);
            void Warning(const char* fmt, ...);
            void Info(const char* fmt, ...);
            void Debug(const char* fmt, ...);
            void Trace(const char* fmt, ...);

        private:
            class LoggerImpl;
            std::unique_ptr<LoggerImpl> logger_impl_;
        };
    } //log
} //zt

#endif //LOG_LOGGER_H
