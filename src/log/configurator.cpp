#include "boost/log/trivial.hpp"
#include "boost/log/utility/setup.hpp"
#include "log/configurator.h"

using namespace zt::log;
using namespace boost::log;

static const std::string DEFAULT_FILE_PREFIX_NAME = "zeta";
static const std::string DEFAULT_FILE_EXTENSION = ".log";

namespace
{
    trivial::severity_level ToSeverityLevel(LogLevel level)
    {
        switch(level)
        {
            case LogLevel::FATAL:   return trivial::fatal;
            case LogLevel::ERROR:   return trivial::error;
            case LogLevel::WARNING: return trivial::warning;
            case LogLevel::INFO:    return trivial::info;
            case LogLevel::DEBUG:   return trivial::debug;
            case LogLevel::TRACE:   return trivial::trace;
            default: assert(false);
        }
    }
}

void Configurator::Setup(LogLevel level, OutputType output_type,
                         const std::string& file_name_prefix /* = "" */)
{
    static const std::string COMMON_FMT("[%TimeStamp%] [%ThreadID%] [%Severity%] [%ClassName%]:  %Message%");

    register_simple_formatter_factory< trivial::severity_level, char >("Severity");

    if ((output_type & CONSOLE) == CONSOLE)
    {
        // Output message to console
        add_console_log(
                        std::cout,
                        keywords::format = COMMON_FMT,
                        keywords::auto_flush = true
                        );
    }
    
    if ((output_type & FILE) == FILE)
    {
        std::string file_name = file_name_prefix.empty() ?
            DEFAULT_FILE_PREFIX_NAME : file_name_prefix;
        file_name += "_%m%d%Y_%H%M%S_%3N";
        file_name += DEFAULT_FILE_EXTENSION;
    
        // Output message to file, rotates when file reached 1mb or at midnight every day. Each log file
        // is capped at 1mb and total is 20mb
        add_file_log (
                      keywords::file_name = file_name,
                      keywords::rotation_size = 1 * 1024 * 1024,
                      keywords::max_size = 20 * 1024 * 1024,
                      keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
                      keywords::format = COMMON_FMT,
                      keywords::auto_flush = true
                      );
    }

    add_common_attributes();

    core::get()->set_filter(trivial::severity >= ToSeverityLevel(level));
}
