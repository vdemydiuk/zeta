//
//  description.h
//  zt
//
//  Created by Vyacheslav Demidyuk on 11/5/20.
//

#ifndef SDP_DESCRIPTION_H
#define SDP_DESCRIPTION_H

#include <memory>
#include <string>
#include <vector>
#include "attribute.h"
#include "bandwidth.h"
#include "connection_data.h"

namespace zt
{
    namespace sdp
    {
        class Description
        {
        public:
            virtual ~Description() {}
            
            // ("c=")
            std::shared_ptr<ConnectionData> getConnection() const;
            // ("b=")
            std::shared_ptr<Bandwidth> getBandwidth() const;
            
            void setConnection(const std::shared_ptr<ConnectionData>& connection);
            void setBandwidth(const std::shared_ptr<Bandwidth>& bandwidth);
            
            void addAttribute(const std::shared_ptr<Attribute>& attr);
            void addAttribute(const std::string& attr);
            void addAttribute(const std::string& attr, const std::string& value);
            
            uint32_t getAttributeCount() const;
            
            template<typename T = Attribute>
            std::shared_ptr<T> getAttribute()
            {
                //For each attribute
                for (auto attr : attributes_)
                {
                    //Try to convert it
                    auto t = std::dynamic_pointer_cast<T>(attr);
                    //If it is from this type
                    if (t)
                        //Found
                        return t;
                }
                //Not found
                return std::shared_ptr<T>();
            }

            std::shared_ptr<Attribute> getAttribute(uint32_t index)
            {
                return index < attributes_.size() ? attributes_[index] : nullptr;
            }
            
            template<typename T = Attribute>
            std::vector<std::shared_ptr<T>> getAttributes()
            {
                //Create list
                auto attrs = std::vector<std::shared_ptr<T>>();
                //For each attribute
                for (auto attr : attributes_)
                {
                    //Try to convert it
                    auto t = std::dynamic_pointer_cast<T>(attr);
                    //If it was that type
                    if (t)
                        //Add it
                        attrs.push_back(t);
                }
                //Done
                return attrs;
            }
            
            virtual std::string toString() const;
            
        private:
            typedef std::vector<std::shared_ptr<Attribute>> AttributeVector;
            
            std::shared_ptr<ConnectionData> connection_;
            std::shared_ptr<Bandwidth>      bandwidth_;
            AttributeVector                 attributes_;
        };
    }
}

#endif /* SDP_DESCRIPTION_H */
