#include "mediaengine/media_session.h"
#include "sdp/sdp_parser.h"
#include "api/set_local_description_observer_interface.h"
#include "api/set_remote_description_observer_interface.h"
#include "mediaengine/peerconnection_factory.h"
#include <mutex>
#include <condition_variable>

using namespace zt;
using namespace zt::log;

namespace 
{
    class SetLocalSessionDescriptionObserver
        : public webrtc::SetLocalDescriptionObserverInterface
    {
    public:
        SetLocalSessionDescriptionObserver(log::Logger& log)
        :log_(log)
        {}

        virtual void OnSetLocalDescriptionComplete(webrtc::RTCError error)
        {
            if (error.ok())
                log_.Trace("%s: set local session description success.", __FUNCTION__);
            else
                log_.Error("%s: Failed to set local session description! %s:%s", __FUNCTION__, ToString(error.type()), error.message());
        }
    private:
        log::Logger& log_;
    };

    class SetRemoteSessionDescriptionObserver
        : public webrtc::SetRemoteDescriptionObserverInterface
    {
        typedef std::function<void (const webrtc::RTCError&)> Callback;

    public:
        SetRemoteSessionDescriptionObserver(log::Logger& log, Callback callback)
        :callback_(std::move(callback))
        {}

        void OnSetRemoteDescriptionComplete(webrtc::RTCError error) override
        {
            callback_(error);
        }
    private:
        Callback callback_;
    };

    class SessionDescriptionObserver : public webrtc::CreateSessionDescriptionObserver
    {
        typedef std::function<void (webrtc::SessionDescriptionInterface*, const webrtc::RTCError&)> Callback;

    public:
        SessionDescriptionObserver(log::Logger& log, Callback callback)
        :log_(log)
        ,callback_(callback)
        {}

    private:
        // CreateSessionDescriptionObserver implementation.
        void OnSuccess(webrtc::SessionDescriptionInterface* desc) override 
        {
            webrtc::RTCError error;
            callback_(desc, error);
        }

        void OnFailure(webrtc::RTCError error) override
        {
            callback_(nullptr, error);
        }

        log::Logger& log_;
        Callback callback_;
    };
}

CreateSessionDescRequest::CreateSessionDescRequest(
    rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection,
    rtc::scoped_refptr<webrtc::SetLocalDescriptionObserverInterface> observer,
    Callback callback)
:peer_connection_(peer_connection)
,set_desc_observer(observer)
,callback_(std::move(callback))
{}

void CreateSessionDescRequest::OnIceGatheringComplete()
{
    callback_(desc_);
}

void CreateSessionDescRequest::OnSuccess(webrtc::SessionDescriptionInterface* desc) 
{
    desc_ = desc;
    std::unique_ptr<webrtc::SessionDescriptionInterface> desc_ptr(desc);
    peer_connection_->SetLocalDescription(std::move(desc_ptr), set_desc_observer);
}

void CreateSessionDescRequest::OnFailure(webrtc::RTCError error)
{
    callback_(nullptr);
}

MediaSession::MediaSession(PeerConnectionFactory& peer_connection_factory)
:log_("MediaSession")
{
    peer_connection_ = peer_connection_factory.CreatePeerConnection(this);
}

MediaSession::~MediaSession()
{
    Close();
}

void MediaSession::CreateSessionDescription(CreateSessionCallback callback)
{
    log_.Trace("%s", __FUNCTION__);

    auto create_session_callback = [callback](webrtc::SessionDescriptionInterface* desc)
    {
        std::string sdp;
        desc->ToString(&sdp);
        auto session_description = std::make_unique<sdp::SessionDescription>();
        sdp::SdpParser::Parse(sdp, *session_description.get());
        callback(std::move(session_description));
    };

    create_session_desc_request_ = rtc::scoped_refptr<CreateSessionDescRequest>(
        new rtc::RefCountedObject<CreateSessionDescRequest>(peer_connection_,
            new rtc::RefCountedObject<SetLocalSessionDescriptionObserver>(log_),
            std::move(create_session_callback)));
    webrtc::PeerConnectionInterface::RTCOfferAnswerOptions options;
    options.offer_to_receive_audio = true;
    options.offer_to_receive_video = true;
    options.use_rtp_mux = false;
    peer_connection_->CreateOffer(create_session_desc_request_.get(), options);
}

bool MediaSession::Update(const sdp::SessionDescription& remote_sdp)
{
    log_.Trace("%s", __FUNCTION__);

    if (peer_connection_ == nullptr)
    {
        log_.Warning("%s: Failed: peer connection is not defined!", __FUNCTION__);
        return false;
    }

    std::mutex mtx;
    std::condition_variable signal;
    bool result_ready{false};
    webrtc::RTCError result_error;
    auto callback = [&result_error, &result_ready, &signal, &mtx](const webrtc::RTCError& error)
    {
        std::lock_guard<std::mutex> _(mtx);
        result_error = error;
        result_ready = true;
        signal.notify_one();
    };

    webrtc::SdpParseError error;
    std::unique_ptr<webrtc::SessionDescriptionInterface> remote_desc  =
            webrtc::CreateSessionDescription(webrtc::SdpType::kAnswer, remote_sdp.toString(), &error);
    peer_connection_->SetRemoteDescription(std::move(remote_desc), new rtc::RefCountedObject<SetRemoteSessionDescriptionObserver>(log_, callback));

    {
        std::unique_lock<std::mutex> lock(mtx);
        signal.wait(lock, [&result_ready]{ return result_ready; });
    }
    if (!result_error.ok())
        log_.Error("%s: Failed to set remote session description! %s:%s", __FUNCTION__, ToString(result_error.type()), result_error.message());
    return result_error.ok();
}

void MediaSession::Close()
{
    if (peer_connection_)
    {
        peer_connection_->Close();
        peer_connection_ = nullptr;
    }
}

void MediaSession::OnIceGatheringChange(
    webrtc::PeerConnectionInterface::IceGatheringState new_state)
{
    log_.Trace("%s: %d", __FUNCTION__, new_state);

    if (new_state == webrtc::PeerConnectionInterface::kIceGatheringComplete)
    {
        if (create_session_desc_request_)
            create_session_desc_request_->OnIceGatheringComplete();
    }
}

void MediaSession::OnIceCandidate(const webrtc::IceCandidateInterface* candidate)
{
    std::string sdp;
    candidate->ToString(&sdp);
    log_.Trace("%s: %s", __FUNCTION__, sdp.c_str());
}