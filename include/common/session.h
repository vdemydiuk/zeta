#ifndef SESSION_H
#define SESSION_H

#include <functional>

namespace zt
{
    namespace sdp
    {
        class SessionDescription;
    }

    class Session
    {
    public:
        typedef std::function<void (std::unique_ptr<sdp::SessionDescription>)> CreateSessionCallback;
        
        virtual ~Session() {}
        virtual void CreateSessionDescription(CreateSessionCallback callback) = 0;
        virtual bool Update(const sdp::SessionDescription& remote_sdp) = 0;
        virtual void Close() = 0;
    };
}

#endif // SESSION_H