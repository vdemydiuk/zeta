cmake_minimum_required(VERSION 3.1)

add_subdirectory(mediaengine)
add_subdirectory(sdp)
add_subdirectory(sip)
