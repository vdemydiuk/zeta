#ifndef SESSION_PROVIDER_H
#define SESSION_PROVIDER_H

namespace zt
{
    class Session;

    struct SessionProvider
    {
        virtual std::unique_ptr<Session> CreateSession() = 0;
    };
}

#endif //SESSION_PROVIDER_H