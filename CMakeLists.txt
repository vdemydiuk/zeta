project(zeta CXX)
cmake_minimum_required(VERSION 3.1)

set( CMAKE_CXX_STANDARD 14 )

# set CMAKE_MODULE_PATH for cmake macro/function and modules
set( CMAKE_MODULE_PATH
  ${CMAKE_MODULE_PATH}
  ${CMAKE_CURRENT_SOURCE_DIR}/cmake
)

set( BUILD_TYPE ${CMAKE_BUILD_TYPE} )
if( NOT INSTALL_DEPENDENCIES_DIR )
    set( INSTALL_DEPENDENCIES_DIR ${CMAKE_BINARY_DIR}/install CACHE STRING "Install directory for dependencies")
endif()

include( ExternalProject )

include( External-Boost )
include( External-PJSIP )

set ( WEBRTC_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/webrtc )

if ( APPLE )
  set( WEBRTC_LIBRARY_DIR ${CMAKE_CURRENT_SOURCE_DIR}/libs/webrtc/macos )
else()
  set( WEBRTC_LIBRARY_DIR ${CMAKE_CURRENT_SOURCE_DIR}/libs/webrtc/linux )
endif()

set ( WEBRTC_LABRARIES 
  ${WEBRTC_LIBRARY_DIR}/libwebrtc.a
  -lX11
  -ldl
  -lglib-2.0
  -latomic
)

add_compile_definitions(WEBRTC_LINUX)
add_compile_definitions(WEBRTC_POSIX)

add_subdirectory(src)
add_subdirectory(apps)
add_subdirectory(thirdparty)
add_subdirectory(tests)
