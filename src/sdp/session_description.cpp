//
//  session_description.cpp
//  zt
//
//  Created by Vyacheslav Demidyuk on 10/7/20.
//

#include "sdp/session_description.h"
#include <sstream>

using namespace zt::sdp;

uint32_t SessionDescription::getVersion() const
{
    return version_;
}

std::string SessionDescription::getSessionName() const
{
    return session_name_;
}

std::shared_ptr<Origin> SessionDescription::getOrigin() const
{
    return origin_;
}

std::string SessionDescription::getInformation() const
{
    return information_;
}

std::string SessionDescription::getUri() const
{
    return uri_;
}

std::string SessionDescription::getEmail() const
{
    return email_;
}

std::shared_ptr<Timing> SessionDescription::getTiming() const
{
    return timing_;
}

uint32_t SessionDescription::getMediaCount() const
{
    return media_descriptions_.size();
}

std::shared_ptr<MediaDescription> SessionDescription::getMedia(uint32_t index) const
{
    return index < media_descriptions_.size() ? media_descriptions_[index] : nullptr;
}

void SessionDescription::setVersion(uint32_t version)
{
    version_ = version;
}

void SessionDescription::setSessionName(const std::string& name)
{
    session_name_ = name;
}

void SessionDescription::setOrigin(const std::shared_ptr<Origin>& origin)
{
    origin_ = origin;
}

void SessionDescription::setInformation(const std::string& info)
{
    information_ = info;
}

void SessionDescription::setUri(const std::string& uri)
{
    uri_ = uri;
}

void SessionDescription::setEmail(const std::string &email)
{
    email_ = email;
}

void SessionDescription::setTiming(const std::shared_ptr<Timing>& timing)
{
    timing_ = timing;
}

void SessionDescription::addMediaDescription(const std::shared_ptr<MediaDescription>& media)
{
    if (media)
        media_descriptions_.push_back(media);
}

std::string SessionDescription::toString() const
{
    std::stringstream value;
    
    value << "v=" + std::to_string(version_) + "\r\n";
    if (origin_)
        value << origin_->toString();
    value << "s=" << session_name_ << "\r\n";
    if (!uri_.empty())
        value << "u=" << uri_ << "\r\n";
    if (!email_.empty())
        value << "e=" << email_ << "\r\n";
    if (!information_.empty())
        value << "i=" << information_ << "\r\n";
    if (timing_)
        value << timing_->toString();
    value << Description::toString();
    for (auto media : media_descriptions_)
        value << media->toString();

    return value.str();
}
