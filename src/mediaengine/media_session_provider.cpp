//
//  media_session_provider.cpp
//  MediaSessionProvider
//
//  Created by Vyacheslav Demidyuk on 4/28/21.
//

#include "mediaengine/media_session_provider.h"
#include "mediaengine/media_session.h"
#include "mediaengine/peerconnection_factory.h"

using namespace zt;
using namespace zt::log;

MediaSessionProvider::MediaSessionProvider()
:log_("MediaSessionProvider")
{
    
}

MediaSessionProvider::~MediaSessionProvider()
{
}

bool MediaSessionProvider::Create()
{
    log_.Trace("%s", __FUNCTION__);

    if (peer_connection_factory_)
    {
        log_.Debug("%s: MediaSessionProvider is already initialized!", __FUNCTION__);
        return false;
    }

    peer_connection_factory_ = std::make_unique<PeerConnectionFactory>();
    return peer_connection_factory_ != nullptr;
}

void MediaSessionProvider::Destroy()
{
    log_.Trace("%s", __FUNCTION__);

    peer_connection_factory_ = nullptr;
}

std::unique_ptr<Session> MediaSessionProvider::CreateSession()
{
    log_.Trace("%s", __FUNCTION__);

    auto session = std::make_unique<MediaSession>(*peer_connection_factory_.get());
    return session;
}