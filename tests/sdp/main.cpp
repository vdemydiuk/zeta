#include "gtest/gtest.h"
#include "sdp/session_description.h"
#include "sdp/sdp_parser.h"

using namespace zt;
using namespace zt::sdp;

namespace
{
    TEST(SdpGenericAttribute, TestToString)
    {
        GenericAttribute attr("attribute", "value");
        std::string sdp_line = attr.toString();
        EXPECT_EQ(sdp_line, "a=attribute:value\r\n");
    }
    
    TEST(SdpRtpMapAttribute, TestToString)
    {
        {// test without paramters
            RtpMapAttr attr(99, "h263-1998", 90000);
            std::string sdp_line = attr.toString();
            EXPECT_EQ(sdp_line, "a=rtpmap:99 h263-1998/90000\r\n");
        }
        
        {// test with parameters
            RtpMapAttr attr(99, "AMR-WB", 16000, "1");
            std::string sdp_line = attr.toString();
            EXPECT_EQ(sdp_line, "a=rtpmap:99 AMR-WB/16000/1\r\n");
        }
    }
    
    TEST(SdpDirectionAttr, TestToString)
    {
        DirectionAttr inactive_attr(DirectionType::INACTIVE);
        std::string inactive_attr_line = inactive_attr.toString();
        EXPECT_EQ(inactive_attr_line, "a=inactive\r\n");
        
        DirectionAttr recvonly_attr(DirectionType::RECVONLY);
        std::string recvonly_attr_line = recvonly_attr.toString();
        EXPECT_EQ(recvonly_attr_line, "a=recvonly\r\n");
        
        DirectionAttr sendonly_attr(DirectionType::SENDONLY);
        std::string sendonly_attr_line = sendonly_attr.toString();
        EXPECT_EQ(sendonly_attr_line, "a=sendonly\r\n");
        
        DirectionAttr sendrecv_attr(DirectionType::SENDRECV);
        std::string sendrecv_attr_line = sendrecv_attr.toString();
        EXPECT_EQ(sendrecv_attr_line, "a=sendrecv\r\n");
    }
    
    TEST(SdpFormatAttr, TestToString)
    {
        FormatAttr attr(99, "profile-level-id=4d0028;packetization-mode=1");
        std::string sdp_line = attr.toString();
        EXPECT_EQ(sdp_line, "a=fmtp:99 profile-level-id=4d0028;packetization-mode=1\r\n");
    }
    
    TEST(SdpCryproAttr, TestToString)
    {
        const std::string sdp = "a=crypto:1 AES_CM_128_HMAC_SHA1_80 inline:CoKH4lo5t34SYU0pqeJGwes2gJCEWKFmLUv/q0sN|2^20|1:4"
                            " FEC_ORDER=SRTP_FEC KDR=2 WSH=4 UNENCRYPTED_SRTP UNENCRYPTED_SRTCP UNAUTHENTICATED_SRTP\r\n";
        
        CryptoAttr attr(1, "AES_CM_128_HMAC_SHA1_80");
        auto crypto_key = std::make_shared<CryptoAttr::Key>("inline", "CoKH4lo5t34SYU0pqeJGwes2gJCEWKFmLUv/q0sN");
        crypto_key->setLifetime(std::make_shared<CryptoAttr::Lifetime>(20));
        crypto_key->setMKI(std::make_shared<CryptoAttr::MKI>(1,4));
        attr.addKey(crypto_key);
        attr.addParam(std::make_shared<CryptoAttr::FecOrderParam>(FecOrderType::SRTP_FEC));
        attr.addParam(std::make_shared<CryptoAttr::IntParam>("KDR", 2));
        attr.addParam(std::make_shared<CryptoAttr::IntParam>("WSH", 4));
        attr.addParam(std::make_shared<CryptoAttr::SrtpParam>("UNENCRYPTED_SRTP"));
        attr.addParam(std::make_shared<CryptoAttr::SrtpParam>("UNENCRYPTED_SRTCP"));
        attr.addParam(std::make_shared<CryptoAttr::SrtpParam>("UNAUTHENTICATED_SRTP"));
        
        std::string sdp_line = attr.toString();
        EXPECT_EQ(sdp_line, sdp);
    }
    
    TEST(SdpCryproAttr, TestToString_TwoKeys)
    {
        const std::string sdp = "a=crypto:2 F8_128_HMAC_SHA1_80"
                          " inline:MTIzNDU2Nzg5QUJDREUwMTIzNDU2Nzg5QUJjZGVm|2^20|1:4;"
                          " inline:QUJjZGVmMTIzNDU2Nzg5QUJDREUwMTIzNDU2Nzg5|2^20|2:4\r\n";
        
        CryptoAttr attr(2, "F8_128_HMAC_SHA1_80");
        auto crypto_key1 = std::make_shared<CryptoAttr::Key>("inline", "MTIzNDU2Nzg5QUJDREUwMTIzNDU2Nzg5QUJjZGVm");
        crypto_key1->setLifetime(std::make_shared<CryptoAttr::Lifetime>(20));
        crypto_key1->setMKI(std::make_shared<CryptoAttr::MKI>(1,4));
        attr.addKey(crypto_key1);
        auto crypto_key2 = std::make_shared<CryptoAttr::Key>("inline", "QUJjZGVmMTIzNDU2Nzg5QUJDREUwMTIzNDU2Nzg5");
        crypto_key2->setLifetime(std::make_shared<CryptoAttr::Lifetime>(20));
        crypto_key2->setMKI(std::make_shared<CryptoAttr::MKI>(2,4));
        attr.addKey(crypto_key2);
        
        std::string sdp_line = attr.toString();
        EXPECT_EQ(sdp_line, sdp);
    }
    
    TEST(SdpBandwidth, TestToString)
    {
        Bandwidth bandwidth_as(BandwidthType::AS, 1152);
        std::string as_sdp_line = bandwidth_as.toString();
        EXPECT_EQ(as_sdp_line, "b=AS:1152\r\n");
        
        Bandwidth bandwidth_ct(BandwidthType::CT, 2248);
        std::string ct_sdp_line = bandwidth_ct.toString();
        EXPECT_EQ(ct_sdp_line, "b=CT:2248\r\n");
    }
    
    TEST(SdpConnectionData, TestToString)
    {
        ConnectionData connection("IN", "IP4", "224.2.36.42/127");
        std::string sdp_line = connection.toString();
        EXPECT_EQ(sdp_line, "c=IN IP4 224.2.36.42/127\r\n");
    }
    
    TEST(SdpOrigin, TestToString)
    {
        Origin origin("-", 1, 2, "IN", "IP4", "127.0.0.1");
        std::string sdp_line = origin.toString();
        EXPECT_EQ(sdp_line, "o=- 1 2 IN IP4 127.0.0.1\r\n");
    }
    
    TEST(SdpTiming, TestToString)
    {
        Timing timing(0, 0);
        std::string sdp_line = timing.toString();
        EXPECT_EQ(sdp_line, "t=0 0\r\n");
    }
    
    TEST(SdpMediaDescription_OneFormat, TestToString)
    {
        const std::string sdp = "m=audio 49232 RTP/AVP 0\r\n";
        
        MediaDescription media(MediaType::AUDIO, 49232);
        media.setProto("RTP/AVP");
        media.addFormat(0);
        std::string sdp_line = media.toString();
        EXPECT_EQ(sdp_line, sdp);
    }
    
    TEST(MediaDescription_WithAttributes, TestToString)
    {
        const std::string sdp = "m=video 62359 RTP/AVP 98 99 120\r\n"
                                "c=IN IP4 224.2.36.42\r\n"
                                "a=rtpmap:98 h263-1998/90000\r\n"
                                "a=rtpmap:99 H264/90000\r\n"
                                "a=fmtp:99 profile-level-id=4d0028;packetization-mode=1\r\n"
                                "a=rtpmap:120 VP8/90000\r\n";
        
        MediaDescription media(MediaType::VIDEO, 62359);
        media.setProto("RTP/AVP");
        media.addFormat(98);
        media.addFormat(99);
        media.addFormat(120);
        media.setConnection(std::make_shared<ConnectionData>("IN", "IP4", "224.2.36.42"));
        media.addAttribute(std::make_shared<RtpMapAttr>(98, "h263-1998", 90000));
        media.addAttribute(std::make_shared<RtpMapAttr>(99, "H264", 90000));
        media.addAttribute(std::make_shared<FormatAttr>(99, "profile-level-id=4d0028;packetization-mode=1"));
        media.addAttribute(std::make_shared<RtpMapAttr>(120, "VP8", 90000));
        std::string sdp_line = media.toString();
        EXPECT_EQ(sdp_line, sdp);
    }
    
    TEST(SessionDescription, TestToString)
    {
        const std::string sdp = "v=0\r\n"
        "o=- 1 2 IN IP4 127.0.0.1\r\n"
        "s=webrtc (chrome 22.0.1189.0)\r\n"
        "u=https://foo.com\r\n"
        "e=alice@foo.com\r\n"
        "i=This test is for sdp\r\n"
        "t=0 0\r\n"
        "c=IN IP4 224.2.36.42/127\r\n"
        "b=AS:4680\r\n"
        "m=audio 49232/2 RTP/AVP 0\r\n"
        "m=video 62359 RTP/AVP 98 99 120\r\n"
        "c=IN IP4 224.2.36.42\r\n"
        "b=AS:30\r\n"
        "a=rtpmap:98 h263-1998/90000\r\n"
        "a=rtpmap:99 H264/90000\r\n"
        "a=fmtp:99 profile-level-id=4d0028;packetization-mode=1\r\n"
        "a=rtpmap:120 VP8/90000\r\n"
        "a=crypto:1 AES_CM_128_HMAC_SHA1_80 inline:Eyo5IxsCkN9u9y1qjpBUIPfLDNv4ic4qCGz4vv9D|2^48\r\n";
        
        SessionDescription session_description;
        session_description.setSessionName("webrtc (chrome 22.0.1189.0)");
        session_description.setUri("https://foo.com");
        session_description.setEmail("alice@foo.com");
        session_description.setInformation("This test is for sdp");
        
        auto origin = std::make_shared<Origin>("-", 1, 2, "IN", "IP4", "127.0.0.1");
        session_description.setOrigin(origin);
        
        auto timing = std::make_shared<Timing>(0, 0);
        session_description.setTiming(timing);
        
        auto connection = std::make_shared<ConnectionData>("IN", "IP4", "224.2.36.42", 127);
        session_description.setConnection(connection);
        
        auto bandwidth = std::make_shared<Bandwidth>(BandwidthType::AS, 4680);
        session_description.setBandwidth(bandwidth);
        
        auto audio_media = std::make_shared<MediaDescription>(MediaType::AUDIO, 49232);
        audio_media->setNumPorts(2);
        audio_media->setProto("RTP/AVP");
        audio_media->addFormat(0);
        session_description.addMediaDescription(audio_media);

        auto video_media = std::make_shared<MediaDescription>(MediaType::VIDEO, 62359);
        video_media->setProto("RTP/AVP");
        video_media->addFormat(98);
        video_media->addFormat(99);
        video_media->addFormat(120);
        video_media->setConnection(std::make_shared<ConnectionData>("IN", "IP4", "224.2.36.42"));
        video_media->setBandwidth(std::make_shared<Bandwidth>(BandwidthType::AS, 30));
        video_media->addAttribute(std::make_shared<RtpMapAttr>(98, "h263-1998", 90000));
        video_media->addAttribute(std::make_shared<RtpMapAttr>(99, "H264", 90000));
        video_media->addAttribute(std::make_shared<FormatAttr>(99, "profile-level-id=4d0028;packetization-mode=1"));
        video_media->addAttribute(std::make_shared<RtpMapAttr>(120, "VP8", 90000));
        auto video_media_crypto_attr = std::make_shared<CryptoAttr>(1, "AES_CM_128_HMAC_SHA1_80");
        auto video_media_crypto_key = std::make_shared<CryptoAttr::Key>("inline", "Eyo5IxsCkN9u9y1qjpBUIPfLDNv4ic4qCGz4vv9D");
        video_media_crypto_key->setLifetime(std::make_shared<CryptoAttr::Lifetime>(48));
        video_media_crypto_attr->addKey(video_media_crypto_key);
        video_media->addAttribute(video_media_crypto_attr);
        session_description.addMediaDescription(video_media);
        
        std::string sdp_line = session_description.toString();
        EXPECT_EQ(sdp_line, sdp);
    }
    
    TEST(SessionDescription, TestParse)
    {
        const std::string sdp = "v=0\r\n"
        "o=- 1 2 IN IP4 127.0.0.1\r\n"
        "s=webrtc (chrome 22.0.1189.0)\r\n"
        "u=https://foo.com\r\n"
        "e=alice@foo.com\r\n"
        "i=This test is for sdp\r\n"
        "c=IN IP4 224.2.36.42/127/3\r\n"
        "b=AS:4680\r\n"
        "t=111 2222\r\n"
        "m=audio 49232/2 RTP/AVP 0 110\r\n"
        "a=rtpmap:110 opus/48000/2\r\n"
        "a=sendrecv\r\n"
        "m=video 62359 RTP/AVP 98 99 120\r\n"
        "c=IN IP4 224.2.36.42\r\n"
        "b=TIAS:3300\r\n"
        "a=rtpmap:98 h263-1998/90000\r\n"
        "a=rtpmap:99 H264/90000\r\n"
        "a=fmtp:99 profile-level-id=4d0028;packetization-mode=1\r\n"
        "a=rtpmap:120 VP8/90000\r\n"
        "a=sendonlyr\n"
        "a=crypto:1 AES_CM_128_HMAC_SHA1_80 inline:Eyo5IxsCkN9u9y1qjpBUIPfLDNv4ic4qCGz4vv9D|2^48|1:4\r\n"
        "m=text 50400 RTP/SAVP 98\r\n"
        "a=rtpmap:98 t140/1000\r\n"
        "a=inactive\r\n"
        "m=application 28182 UDP/BFCP *\r\n"
        "a=sendrecv\r\n";
        
        SessionDescription session_description;
        SdpParser::Parse(sdp, session_description);
        EXPECT_EQ(session_description.getVersion(), 0);
        EXPECT_EQ(session_description.getSessionName(), "webrtc (chrome 22.0.1189.0)");
        EXPECT_EQ(session_description.getUri(), "https://foo.com");
        EXPECT_EQ(session_description.getEmail(), "alice@foo.com");
        EXPECT_EQ(session_description.getInformation(), "This test is for sdp");

        auto origin = session_description.getOrigin();
        ASSERT_NE(origin, nullptr);
        EXPECT_EQ(origin->getUser(), "-");
        EXPECT_EQ(origin->getSessionId(), 1);
        EXPECT_EQ(origin->getVersion(), 2);
        EXPECT_EQ(origin->getNetType(), "IN");
        EXPECT_EQ(origin->getAddrType(), "IP4");
        EXPECT_EQ(origin->getAddress(), "127.0.0.1");

        auto timing = session_description.getTiming();
        ASSERT_NE(timing, nullptr);
        EXPECT_EQ(timing->getStart(), 111);
        EXPECT_EQ(timing->getStop(), 2222);
        
        auto connection = session_description.getConnection();
        ASSERT_NE(connection, nullptr);
        EXPECT_EQ(connection->getNetType(), "IN");
        EXPECT_EQ(connection->getAddrType(), "IP4");
        EXPECT_EQ(connection->getAddr(), "224.2.36.42");
        EXPECT_EQ(connection->getTtl(), 127);
        EXPECT_EQ(connection->getNumAddr(), 3);
        
        auto bandwidth = session_description.getBandwidth();
        ASSERT_NE(bandwidth, nullptr);
        EXPECT_EQ(bandwidth->getType(), BandwidthType::AS);
        EXPECT_EQ(bandwidth->getBandwidth(), 4680);
        
        auto media_count = session_description.getMediaCount();
        ASSERT_EQ(media_count, 4);
        
        auto media0 = session_description.getMedia(0);
        ASSERT_NE(media0, nullptr);
        EXPECT_EQ(media0->getMediaType(), MediaType::AUDIO);
        EXPECT_EQ(media0->getPort(), 49232);
        EXPECT_EQ(media0->getNumPorts(), 2);
        EXPECT_EQ(media0->getProto(), "RTP/AVP");
        auto media0_formats = media0->getFormats();
        ASSERT_EQ(media0_formats.size(), 2);
        EXPECT_EQ(media0_formats[0], "0");
        EXPECT_EQ(media0_formats[1], "110");
        EXPECT_EQ(media0->getAttributeCount(), 2);
        auto media0_rtpmap_attributes = media0->getAttributes<RtpMapAttr>();
        EXPECT_EQ(media0_rtpmap_attributes.size(), 1);
        EXPECT_EQ(media0_rtpmap_attributes[0]->getPType(), 110);
        EXPECT_EQ(media0_rtpmap_attributes[0]->getEncoderName(), "opus");
        EXPECT_EQ(media0_rtpmap_attributes[0]->getClockRate(), 48000);
        EXPECT_EQ(media0_rtpmap_attributes[0]->getParameters(), "2");
        auto media0_direction_attr = media0->getAttribute<DirectionAttr>();
        ASSERT_NE(media0_direction_attr, nullptr);
        EXPECT_EQ(media0_direction_attr->getDirection(), DirectionType::SENDRECV);
        
        auto media1 = session_description.getMedia(1);
        ASSERT_NE(media1, nullptr);
        EXPECT_EQ(media1->getMediaType(), MediaType::VIDEO);
        EXPECT_EQ(media1->getPort(), 62359);
        EXPECT_EQ(media1->getProto(), "RTP/AVP");
        auto media1_formats = media1->getFormats();
        ASSERT_EQ(media1_formats.size(), 3);
        EXPECT_EQ(media1_formats[0], "98");
        EXPECT_EQ(media1_formats[1], "99");
        EXPECT_EQ(media1_formats[2], "120");
        auto media1_connection = media1->getConnection();
        ASSERT_NE(media1_connection, nullptr);
        EXPECT_EQ(media1_connection->getNetType(), "IN");
        EXPECT_EQ(media1_connection->getAddrType(), "IP4");
        EXPECT_EQ(media1_connection->getAddr(), "224.2.36.42");
        auto media1_bandwidth = media1->getBandwidth();
        ASSERT_NE(media1_bandwidth, nullptr);
        EXPECT_EQ(media1_bandwidth->getType(), BandwidthType::TIAS);
        EXPECT_EQ(media1_bandwidth->getBandwidth(), 3300);
        EXPECT_EQ(media1->getAttributeCount(), 6);
        auto media1_rtpmap_attributes = media1->getAttributes<RtpMapAttr>();
        EXPECT_EQ(media1_rtpmap_attributes.size(), 3);
        EXPECT_EQ(media1_rtpmap_attributes[0]->getPType(), 98);
        EXPECT_EQ(media1_rtpmap_attributes[0]->getEncoderName(), "h263-1998");
        EXPECT_EQ(media1_rtpmap_attributes[0]->getClockRate(), 90000);
        EXPECT_EQ(media1_rtpmap_attributes[1]->getPType(), 99);
        EXPECT_EQ(media1_rtpmap_attributes[1]->getEncoderName(), "H264");
        EXPECT_EQ(media1_rtpmap_attributes[1]->getClockRate(), 90000);
        EXPECT_EQ(media1_rtpmap_attributes[2]->getPType(), 120);
        EXPECT_EQ(media1_rtpmap_attributes[2]->getEncoderName(), "VP8");
        EXPECT_EQ(media1_rtpmap_attributes[2]->getClockRate(), 90000);
        auto media1_format_attributes = media1->getAttributes<FormatAttr>();
        EXPECT_EQ(media1_format_attributes.size(), 1);
        EXPECT_EQ(media1_format_attributes[0]->getPType(), 99);
        EXPECT_EQ(media1_format_attributes[0]->getParameters(), "profile-level-id=4d0028;packetization-mode=1");
        auto media1_direction_attr = media1->getAttribute<DirectionAttr>();
        ASSERT_NE(media1_direction_attr, nullptr);
        EXPECT_EQ(media1_direction_attr->getDirection(), DirectionType::SENDONLY);
        auto media1_crypto_attr = media1->getAttribute<CryptoAttr>();
        ASSERT_NE(media1_crypto_attr, nullptr);
        EXPECT_EQ(media1_crypto_attr->getTag(), 1);
        EXPECT_EQ(media1_crypto_attr->getCryptoSuite(), "AES_CM_128_HMAC_SHA1_80");
        EXPECT_EQ(media1_crypto_attr->getKeyCount(), 1);
        auto media1_crypto_attr_key = media1_crypto_attr->getKey(0);
        ASSERT_NE(media1_crypto_attr_key, nullptr);
        EXPECT_EQ(media1_crypto_attr_key->getMethod(), "inline");
        EXPECT_EQ(media1_crypto_attr_key->getSalt(), "Eyo5IxsCkN9u9y1qjpBUIPfLDNv4ic4qCGz4vv9D");
        auto media1_crypto_attr_key_lifetime = media1_crypto_attr_key->getLifetime();
        ASSERT_NE(media1_crypto_attr_key_lifetime, nullptr);
        EXPECT_EQ(media1_crypto_attr_key_lifetime->getValue(), 48);
        EXPECT_TRUE(media1_crypto_attr_key_lifetime->isExponent());
        auto media1_crypto_attr_key_mki = media1_crypto_attr_key->getMKI();
        ASSERT_NE(media1_crypto_attr_key_mki, nullptr);
        EXPECT_EQ(media1_crypto_attr_key_mki->getValue(), 1);
        EXPECT_EQ(media1_crypto_attr_key_mki->getLength(), 4);
        
        auto media2 = session_description.getMedia(2);
        ASSERT_NE(media2, nullptr);
        EXPECT_EQ(media2->getMediaType(), MediaType::TEXT);
        EXPECT_EQ(media2->getPort(), 50400);
        EXPECT_EQ(media2->getProto(), "RTP/SAVP");
        auto media2_formats = media2->getFormats();
        ASSERT_EQ(media2_formats.size(), 1);
        EXPECT_EQ(media2_formats[0], "98");
        EXPECT_EQ(media2->getAttributeCount(), 2);
        auto media2_rtpmap_attributes = media2->getAttributes<RtpMapAttr>();
        EXPECT_EQ(media2_rtpmap_attributes.size(), 1);
        EXPECT_EQ(media2_rtpmap_attributes[0]->getPType(), 98);
        EXPECT_EQ(media2_rtpmap_attributes[0]->getEncoderName(), "t140");
        EXPECT_EQ(media2_rtpmap_attributes[0]->getClockRate(), 1000);
        auto media2_direction_attr = media2->getAttribute<DirectionAttr>();
        ASSERT_NE(media2_direction_attr, nullptr);
        EXPECT_EQ(media2_direction_attr->getDirection(), DirectionType::INACTIVE);
        
        auto media3 = session_description.getMedia(3);
        ASSERT_NE(media3, nullptr);
        EXPECT_EQ(media3->getMediaType(), MediaType::APPLICATION);
        EXPECT_EQ(media3->getPort(), 28182);
        EXPECT_EQ(media3->getProto(), "UDP/BFCP");
        auto media3_formats = media3->getFormats();
        ASSERT_EQ(media3_formats.size(), 1);
        EXPECT_EQ(media3_formats[0], "*");
        EXPECT_EQ(media3->getAttributeCount(), 1);
        auto media3_direction_attr = media3->getAttribute<DirectionAttr>();
        ASSERT_NE(media3_direction_attr, nullptr);
        EXPECT_EQ(media3_direction_attr->getDirection(), DirectionType::SENDRECV);
    }

    TEST(SessionDescription, TestParseSdpFromWebRtc)
    {
        const std::string sdp = "v=0\r\n"
            "o=- 7485738414671924216 2 IN IP4 127.0.0.1\r\n"
            "s=-\r\n"
            "t=0 0\r\n"
            "a=group:BUNDLE 0 1\r\n"
            "a=extmap-allow-mixed\r\n"
            "a=msid-semantic: WMS stream_id\r\n"
            "m=audio 9 RTP/AVPF 111 63 103 104 9 102 0 8 106 105 13 110 112 113 126\r\n"
            "c=IN IP4 0.0.0.0\r\n"
            "a=rtcp:9 IN IP4 0.0.0.0\r\n"
            "a=ice-ufrag:gxDy\r\n"
            "a=ice-pwd:Bkzp+1jfRCZUIW8WK4fBdydk\r\n"
            "a=ice-options:trickle\r\n"
            "a=mid:0\r\n"
            "a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level\r\n"
            "a=extmap:2 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r\n"
            "a=extmap:3 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01\r\n"
            "a=extmap:4 urn:ietf:params:rtp-hdrext:sdes:mid\r\n"
            "a=sendrecv\r\n"
            "a=msid:stream_id audio_label\r\n"
            "a=rtcp-mux\r\n"
            "a=rtpmap:111 opus/48000/2\r\n"
            "a=rtcp-fb:111 transport-cc\r\n"
            "a=fmtp:111 minptime=10;useinbandfec=1\r\n"
            "a=rtpmap:63 red/48000/2\r\n"
            "a=fmtp:63 111/111\r\n"
            "a=rtpmap:103 ISAC/16000\r\n"
            "a=rtpmap:104 ISAC/32000\r\n"
            "a=rtpmap:9 G722/8000\r\n"
            "a=rtpmap:102 ILBC/8000\r\n"
            "a=rtpmap:0 PCMU/8000\r\n"
            "a=rtpmap:8 PCMA/8000\r\n"
            "a=rtpmap:106 CN/32000\r\n"
            "a=rtpmap:105 CN/16000\r\n"
            "a=rtpmap:13 CN/8000\r\n"
            "a=rtpmap:110 telephone-event/48000\r\n"
            "a=rtpmap:112 telephone-event/32000\r\n"
            "a=rtpmap:113 telephone-event/16000\r\n"
            "a=rtpmap:126 telephone-event/8000\r\n"
            "a=ssrc:3331788239 cname:aV5YFl81nvITTZkz\r\n"
            "a=ssrc:3331788239 msid:stream_id audio_label\r\n"
            "a=ssrc:3331788239 mslabel:stream_id\r\n"
            "a=ssrc:3331788239 label:audio_label\r\n"
            "m=video 9 RTP/AVPF 96 97 98 99 100 101 127 121 125 120 124 107 108 109 35 36 119 118 123\r\n"
            "c=IN IP4 0.0.0.0\r\n"
            "a=rtcp:9 IN IP4 0.0.0.0\r\n"
            "a=ice-ufrag:gxDy\r\n"
            "a=ice-pwd:Bkzp+1jfRCZUIW8WK4fBdydk\r\n"
            "a=ice-options:trickle\r\n"
            "a=mid:1\r\n"
            "a=extmap:14 urn:ietf:params:rtp-hdrext:toffset\r\n"
            "a=extmap:2 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time\r\n"
            "a=extmap:13 urn:3gpp:video-orientation\r\n"
            "a=extmap:3 http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01\r\n"
            "a=extmap:5 http://www.webrtc.org/experiments/rtp-hdrext/playout-delay\r\n"
            "a=extmap:6 http://www.webrtc.org/experiments/rtp-hdrext/video-content-type\r\n"
            "a=extmap:7 http://www.webrtc.org/experiments/rtp-hdrext/video-timing\r\n"
            "a=extmap:8 http://www.webrtc.org/experiments/rtp-hdrext/color-space\r\n"
            "a=extmap:4 urn:ietf:params:rtp-hdrext:sdes:mid\r\n"
            "a=extmap:10 urn:ietf:params:rtp-hdrext:sdes:rtp-stream-id\r\n"
            "a=extmap:11 urn:ietf:params:rtp-hdrext:sdes:repaired-rtp-stream-id\r\n"
            "a=sendrecv\r\n"
            "a=msid:stream_id video_label\r\n"
            "a=rtcp-mux\r\n"
            "a=rtcp-rsize\r\n"
            "a=rtpmap:96 VP8/90000\r\n"
            "a=rtcp-fb:96 goog-remb\r\n"
            "a=rtcp-fb:96 transport-cc\r\n"
            "a=rtcp-fb:96 ccm fir\r\n"
            "a=rtcp-fb:96 nack\r\n"
            "a=rtcp-fb:96 nack pli\r\n"
            "a=rtpmap:97 rtx/90000\r\n"
            "a=fmtp:97 apt=96\r\n"
            "a=rtpmap:98 VP9/90000\r\n"
            "a=rtcp-fb:98 goog-remb\r\n"
            "a=rtcp-fb:98 transport-cc\r\n"
            "a=rtcp-fb:98 ccm fir\r\n"
            "a=rtcp-fb:98 nack\r\n"
            "a=rtcp-fb:98 nack pli\r\n"
            "a=fmtp:98 profile-id=0\r\n"
            "a=rtpmap:99 rtx/90000\r\n"
            "a=fmtp:99 apt=98\r\n"
            "a=rtpmap:100 VP9/90000\r\n"
            "a=rtcp-fb:100 goog-remb\r\n"
            "a=rtcp-fb:100 transport-cc\r\n"
            "a=rtcp-fb:100 ccm fir\r\n"
            "a=rtcp-fb:100 nack\r\n"
            "a=rtcp-fb:100 nack pli\r\n"
            "a=fmtp:100 profile-id=2\r\n"
            "a=rtpmap:101 rtx/90000\r\n"
            "a=fmtp:101 apt=100\r\n"
            "a=rtpmap:127 H264/90000\r\n"
            "a=rtcp-fb:127 goog-remb\r\n"
            "a=rtcp-fb:127 transport-cc\r\n"
            "a=rtcp-fb:127 ccm fir\r\n"
            "a=rtcp-fb:127 nack\r\n"
            "a=rtcp-fb:127 nack pli\r\n"
            "a=fmtp:127 level-asymmetry-allowed=1;packetization-mode=1;profile-level-id=42001f\r\n"
            "a=rtpmap:121 rtx/90000\r\n"
            "a=fmtp:121 apt=127\r\n"
            "a=rtpmap:125 H264/90000\r\n"
            "a=rtcp-fb:125 goog-remb\r\n"
            "a=rtcp-fb:125 transport-cc\r\n"
            "a=rtcp-fb:125 ccm fir\r\n"
            "a=rtcp-fb:125 nack\r\n"
            "a=rtcp-fb:125 nack pli\r\n"
            "a=fmtp:125 level-asymmetry-allowed=1;packetization-mode=0;profile-level-id=42001f\r\n"
            "a=rtpmap:120 rtx/90000\r\n"
            "a=fmtp:120 apt=125\r\n"
            "a=rtpmap:124 H264/90000\r\n"
            "a=rtcp-fb:124 goog-remb\r\n"
            "a=rtcp-fb:124 transport-cc\r\n"
            "a=rtcp-fb:124 ccm fir\r\n"
            "a=rtcp-fb:124 nack\r\n"
            "a=rtcp-fb:124 nack pli\r\n"
            "a=fmtp:124 level-asymmetry-allowed=1;packetization-mode=1;profile-level-id=42e01f\r\n"
            "a=rtpmap:107 rtx/90000\r\n"
            "a=fmtp:107 apt=124\r\n"
            "a=rtpmap:108 H264/90000\r\n"
            "a=rtcp-fb:108 goog-remb\r\n"
            "a=rtcp-fb:108 transport-cc\r\n"
            "a=rtcp-fb:108 ccm fir\r\n"
            "a=rtcp-fb:108 nack\r\n"
            "a=rtcp-fb:108 nack pli\r\n"
            "a=fmtp:108 level-asymmetry-allowed=1;packetization-mode=0;profile-level-id=42e01f\r\n"
            "a=rtpmap:109 rtx/90000\r\n"
            "a=fmtp:109 apt=108\r\n"
            "a=rtpmap:35 AV1/90000\r\n"
            "a=rtcp-fb:35 goog-remb\r\n"
            "a=rtcp-fb:35 transport-cc\r\n"
            "a=rtcp-fb:35 ccm fir\r\n"
            "a=rtcp-fb:35 nack\r\n"
            "a=rtcp-fb:35 nack pli\r\n"
            "a=rtpmap:36 rtx/90000\r\n"
            "a=fmtp:36 apt=35\r\n"
            "a=rtpmap:119 red/90000\r\n"
            "a=rtpmap:118 rtx/90000\r\n"
            "a=fmtp:118 apt=119\r\n"
            "a=rtpmap:123 ulpfec/90000\r\n"
            "a=ssrc-group:FID 1618512004 1707477208\r\n"
            "a=ssrc:1618512004 cname:aV5YFl81nvITTZkz\r\n"
            "a=ssrc:1618512004 msid:stream_id video_label\r\n"
            "a=ssrc:1618512004 mslabel:stream_id\r\n"
            "a=ssrc:1618512004 label:video_label\r\n"
            "a=ssrc:1707477208 cname:aV5YFl81nvITTZkz\r\n"
            "a=ssrc:1707477208 msid:stream_id video_label\r\n"
            "a=ssrc:1707477208 mslabel:stream_id\r\n"
            "a=ssrc:1707477208 label:video_label\r\n";

            SessionDescription session_description;
            SdpParser::Parse(sdp, session_description);
            EXPECT_EQ(session_description.getVersion(), 0);
            EXPECT_EQ(session_description.getSessionName(), "-");

            auto origin = session_description.getOrigin();
            ASSERT_NE(origin, nullptr);
            EXPECT_EQ(origin->getUser(), "-");
            EXPECT_EQ(origin->getSessionId(), 7485738414671924216);
            EXPECT_EQ(origin->getVersion(), 2);
            EXPECT_EQ(origin->getNetType(), "IN");
            EXPECT_EQ(origin->getAddrType(), "IP4");
            EXPECT_EQ(origin->getAddress(), "127.0.0.1");

            auto timing = session_description.getTiming();
            ASSERT_NE(timing, nullptr);
            EXPECT_EQ(timing->getStart(), 0);
            EXPECT_EQ(timing->getStop(), 0);

            EXPECT_EQ(session_description.getAttributeCount(), 3);
            auto attr1 = dynamic_cast<GenericAttribute*>(session_description.getAttribute(0).get());
            ASSERT_NE(attr1, nullptr);
            EXPECT_EQ(attr1->getAttribute(), "group");
            EXPECT_EQ(attr1->getValue(), "BUNDLE 0 1");
            auto attr2 = dynamic_cast<GenericAttribute*>(session_description.getAttribute(1).get());
            ASSERT_NE(attr2, nullptr);
            EXPECT_EQ(attr2->getAttribute(), "extmap-allow-mixed");
            EXPECT_EQ(attr2->getValue(), "");
            auto attr3 = dynamic_cast<GenericAttribute*>(session_description.getAttribute(2).get());
            ASSERT_NE(attr3, nullptr);
            EXPECT_EQ(attr3->getAttribute(), "msid-semantic");
            EXPECT_EQ(attr3->getValue(), " WMS stream_id");

            ASSERT_EQ(session_description.getMediaCount(), 2);
            auto audio_media = session_description.getMedia(0);
            ASSERT_NE(audio_media, nullptr);
            EXPECT_EQ(audio_media->getMediaType(), MediaType::AUDIO);
            EXPECT_EQ(audio_media->getPort(), 9);
            EXPECT_EQ(audio_media->getProto(), "RTP/AVPF");
            auto autio_media_formats = audio_media->getFormats();
            ASSERT_EQ(autio_media_formats.size(), 15);
            EXPECT_EQ(autio_media_formats[0], "111"); //111 63 103 104 9 102 0 8 106 105 13 110 112 113 126
            EXPECT_EQ(autio_media_formats[1], "63");
            EXPECT_EQ(autio_media_formats[2], "103");
            EXPECT_EQ(autio_media_formats[3], "104");
            EXPECT_EQ(autio_media_formats[4], "9");
            EXPECT_EQ(autio_media_formats[5], "102");
            EXPECT_EQ(autio_media_formats[6], "0");
            EXPECT_EQ(autio_media_formats[7], "8");
            EXPECT_EQ(autio_media_formats[8], "106");
            EXPECT_EQ(autio_media_formats[9], "105");
            EXPECT_EQ(autio_media_formats[10], "13");
            EXPECT_EQ(autio_media_formats[11], "110");
            EXPECT_EQ(autio_media_formats[12], "112");
            EXPECT_EQ(autio_media_formats[13], "113");
            EXPECT_EQ(autio_media_formats[14], "126");
            auto audio_media_connection = audio_media->getConnection();
            ASSERT_NE(audio_media_connection, nullptr);
            EXPECT_EQ(audio_media_connection->getNetType(), "IN");
            EXPECT_EQ(audio_media_connection->getAddrType(), "IP4");
            EXPECT_EQ(audio_media_connection->getAddr(), "0.0.0.0");

            EXPECT_EQ(audio_media->getAttributeCount(), 34);
            auto audio_media_attr1 = dynamic_cast<GenericAttribute*>(audio_media->getAttribute(0).get());
            ASSERT_NE(audio_media_attr1, nullptr);
            EXPECT_EQ(audio_media_attr1->getAttribute(), "rtcp");
            EXPECT_EQ(audio_media_attr1->getValue(), "9 IN IP4 0.0.0.0");
            auto audio_media_attr2 = dynamic_cast<GenericAttribute*>(audio_media->getAttribute(1).get());
            ASSERT_NE(audio_media_attr2, nullptr);
            EXPECT_EQ(audio_media_attr2->getAttribute(), "ice-ufrag");
            EXPECT_EQ(audio_media_attr2->getValue(), "gxDy");
            auto audio_media_attr3 = dynamic_cast<GenericAttribute*>(audio_media->getAttribute(2).get());
            ASSERT_NE(audio_media_attr3, nullptr);
            EXPECT_EQ(audio_media_attr3->getAttribute(), "ice-pwd");
            EXPECT_EQ(audio_media_attr3->getValue(), "Bkzp+1jfRCZUIW8WK4fBdydk");
            auto audio_media_attr4 = dynamic_cast<GenericAttribute*>(audio_media->getAttribute(3).get());
            ASSERT_NE(audio_media_attr4, nullptr);
            EXPECT_EQ(audio_media_attr4->getAttribute(), "ice-options");
            EXPECT_EQ(audio_media_attr4->getValue(), "trickle");
            auto audio_media_attr5 = dynamic_cast<GenericAttribute*>(audio_media->getAttribute(4).get());
            ASSERT_NE(audio_media_attr5, nullptr);
            EXPECT_EQ(audio_media_attr5->getAttribute(), "mid");
            EXPECT_EQ(audio_media_attr5->getValue(), "0");
            auto audio_media_attr6 = dynamic_cast<GenericAttribute*>(audio_media->getAttribute(5).get());
            ASSERT_NE(audio_media_attr6, nullptr);
            EXPECT_EQ(audio_media_attr6->getAttribute(), "extmap");
            EXPECT_EQ(audio_media_attr6->getValue(), "1 urn:ietf:params:rtp-hdrext:ssrc-audio-level");

            auto res_sdp = session_description.toString();
            EXPECT_EQ(sdp, res_sdp);
    }
    
    TEST(SessionDescription, TestParse_CryptoAttributes)
    {
        const std::string sdp =
        "v=0\r\n"
        "m=audio 49170 RTP/SAVP 0r\n"
        "a=crypto:1 AES_CM_128_HMAC_SHA1_80 inline:WVNfX19zZW1jdGwgKCkgewkyMjA7fQp9CnVubGVz|2^20|1:4 FEC_ORDER=SRTP_FEC KDR=2 WSH=4 UNENCRYPTED_SRTP UNENCRYPTED_SRTCP UNAUTHENTICATED_SRTP\r\n"
        "a=crypto:2 F8_128_HMAC_SHA1_80 inline:MTIzNDU2Nzg5QUJDREUwMTIzNDU2Nzg5QUJjZGVm|2^20|1:4; inline:QUJjZGVmMTIzNDU2Nzg5QUJDREUwMTIzNDU2Nzg5|2^20|2:4 FEC_ORDER=FEC_SRTP\r\n";
        
        SessionDescription session_description;
        SdpParser::Parse(sdp, session_description);
        EXPECT_EQ(session_description.getVersion(), 0);
        
        ASSERT_EQ(session_description.getMediaCount(), 1);
        
        auto media = session_description.getMedia(0);
        ASSERT_NE(media, nullptr);
        EXPECT_EQ(media->getMediaType(), MediaType::AUDIO);
        EXPECT_EQ(media->getPort(), 49170);
        EXPECT_EQ(media->getProto(), "RTP/SAVP");
        auto media_formats = media->getFormats();
        ASSERT_EQ(media_formats.size(), 1);
        
        EXPECT_EQ(media->getAttributeCount(), 2);
        auto media_crypto_attr_vec = media->getAttributes<CryptoAttr>();
        ASSERT_EQ(media_crypto_attr_vec.size(), 2);

        EXPECT_EQ(media_crypto_attr_vec[0]->getTag(), 1);
        EXPECT_EQ(media_crypto_attr_vec[0]->getCryptoSuite(), "AES_CM_128_HMAC_SHA1_80");
        EXPECT_EQ(media_crypto_attr_vec[0]->getKeyCount(), 1);
        auto media_crypto_attr0_key = media_crypto_attr_vec[0]->getKey(0);
        ASSERT_NE(media_crypto_attr0_key, nullptr);
        EXPECT_EQ(media_crypto_attr0_key->getMethod(), "inline");
        EXPECT_EQ(media_crypto_attr0_key->getSalt(), "WVNfX19zZW1jdGwgKCkgewkyMjA7fQp9CnVubGVz");
        auto media_crypto_attr0_key_lifetime = media_crypto_attr0_key->getLifetime();
        ASSERT_NE(media_crypto_attr0_key_lifetime, nullptr);
        EXPECT_EQ(media_crypto_attr0_key_lifetime->getValue(), 20);
        EXPECT_TRUE(media_crypto_attr0_key_lifetime->isExponent());
        auto media_crypto_attr0_key_mki = media_crypto_attr0_key->getMKI();
        ASSERT_NE(media_crypto_attr0_key_mki, nullptr);
        EXPECT_EQ(media_crypto_attr0_key_mki->getValue(), 1);
        EXPECT_EQ(media_crypto_attr0_key_mki->getLength(), 4);
        EXPECT_EQ(media_crypto_attr_vec[0]->getParamCount(), 6);
        auto media_crypto_attr0_param0 = media_crypto_attr_vec[0]->getParam(0);
        ASSERT_NE(media_crypto_attr0_param0, nullptr);
        EXPECT_EQ(media_crypto_attr0_param0->getName(), "FEC_ORDER");
        auto media_crypto_attr0_fec = dynamic_cast<CryptoAttr::FecOrderParam*>(media_crypto_attr0_param0.get());
        ASSERT_NE(media_crypto_attr0_fec, nullptr);
        EXPECT_EQ(media_crypto_attr0_fec->getValue(), FecOrderType::SRTP_FEC);
        auto media_crypto_attr0_param1 = media_crypto_attr_vec[0]->getParam(1);
        ASSERT_NE(media_crypto_attr0_param1, nullptr);
        EXPECT_EQ(media_crypto_attr0_param1->getName(), "KDR");
        auto media_crypto_attr0_kdr = dynamic_cast<CryptoAttr::IntParam*>(media_crypto_attr0_param1.get());
        ASSERT_NE(media_crypto_attr0_kdr, nullptr);
        EXPECT_EQ(media_crypto_attr0_kdr->getValue(), 2);
        auto media_crypto_attr0_param2 = media_crypto_attr_vec[0]->getParam(2);
        ASSERT_NE(media_crypto_attr0_param2, nullptr);
        EXPECT_EQ(media_crypto_attr0_param2->getName(), "WSH");
        auto media_crypto_attr0_wsh = dynamic_cast<CryptoAttr::IntParam*>(media_crypto_attr0_param2.get());
        ASSERT_NE(media_crypto_attr0_wsh, nullptr);
        EXPECT_EQ(media_crypto_attr0_wsh->getValue(), 4);
        auto media_crypto_attr0_param3 = media_crypto_attr_vec[0]->getParam(3);
        ASSERT_NE(media_crypto_attr0_param3, nullptr);
        EXPECT_EQ(media_crypto_attr0_param3->getName(), "UNENCRYPTED_SRTP");
        auto media_crypto_attr0_param4 = media_crypto_attr_vec[0]->getParam(4);
        ASSERT_NE(media_crypto_attr0_param4, nullptr);
        EXPECT_EQ(media_crypto_attr0_param4->getName(), "UNENCRYPTED_SRTCP");
        auto media_crypto_attr0_param5 = media_crypto_attr_vec[0]->getParam(5);
        ASSERT_NE(media_crypto_attr0_param5, nullptr);
        EXPECT_EQ(media_crypto_attr0_param5->getName(), "UNAUTHENTICATED_SRTP");
        
        EXPECT_EQ(media_crypto_attr_vec[1]->getTag(), 2);
        EXPECT_EQ(media_crypto_attr_vec[1]->getCryptoSuite(), "F8_128_HMAC_SHA1_80");
        EXPECT_EQ(media_crypto_attr_vec[1]->getKeyCount(), 2);
        auto media_crypto_attr1_key0 = media_crypto_attr_vec[1]->getKey(0);
        ASSERT_NE(media_crypto_attr1_key0, nullptr);
        EXPECT_EQ(media_crypto_attr1_key0->getMethod(), "inline");
        EXPECT_EQ(media_crypto_attr1_key0->getSalt(), "MTIzNDU2Nzg5QUJDREUwMTIzNDU2Nzg5QUJjZGVm");
        auto media_crypto_attr1_key0_lifetime = media_crypto_attr1_key0->getLifetime();
        ASSERT_NE(media_crypto_attr1_key0_lifetime, nullptr);
        EXPECT_EQ(media_crypto_attr1_key0_lifetime->getValue(), 20);
        EXPECT_TRUE(media_crypto_attr1_key0_lifetime->isExponent());
        auto media_crypto_attr1_key0_mki = media_crypto_attr1_key0->getMKI();
        ASSERT_NE(media_crypto_attr1_key0_mki, nullptr);
        EXPECT_EQ(media_crypto_attr1_key0_mki->getValue(), 1);
        EXPECT_EQ(media_crypto_attr1_key0_mki->getLength(), 4);
        auto media_crypto_attr1_key1 = media_crypto_attr_vec[1]->getKey(1);
        ASSERT_NE(media_crypto_attr1_key1, nullptr);
        EXPECT_EQ(media_crypto_attr1_key1->getMethod(), "inline");
        EXPECT_EQ(media_crypto_attr1_key1->getSalt(), "QUJjZGVmMTIzNDU2Nzg5QUJDREUwMTIzNDU2Nzg5");
        auto media_crypto_attr1_key1_lifetime = media_crypto_attr1_key1->getLifetime();
        ASSERT_NE(media_crypto_attr1_key1_lifetime, nullptr);
        EXPECT_EQ(media_crypto_attr1_key1_lifetime->getValue(), 20);
        EXPECT_TRUE(media_crypto_attr1_key1_lifetime->isExponent());
        auto media_crypto_attr1_key1_mki = media_crypto_attr1_key1->getMKI();
        ASSERT_NE(media_crypto_attr1_key1_mki, nullptr);
        EXPECT_EQ(media_crypto_attr1_key1_mki->getValue(), 2);
        EXPECT_EQ(media_crypto_attr1_key1_mki->getLength(), 4);
        EXPECT_EQ(media_crypto_attr_vec[1]->getParamCount(), 1);
        auto media_crypto_attr1_param0 = media_crypto_attr_vec[1]->getParam(0);
        ASSERT_NE(media_crypto_attr1_param0, nullptr);
        EXPECT_EQ(media_crypto_attr1_param0->getName(), "FEC_ORDER");
        auto media_crypto_attr1_fec = dynamic_cast<CryptoAttr::FecOrderParam*>(media_crypto_attr1_param0.get());
        ASSERT_NE(media_crypto_attr1_fec, nullptr);
        EXPECT_EQ(media_crypto_attr1_fec->getValue(), FecOrderType::FEC_SRTP);
    }
}//namespace

int main()
{
    ::testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}
