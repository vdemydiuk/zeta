project(log CXX)
cmake_minimum_required(VERSION 3.1)

set( LIB_NAME log )

set(LIB_HEADERS_PATH ../../include/log)

set(LIB_HEADERS
    ${LIB_HEADERS_PATH}/configurator.h
    ${LIB_HEADERS_PATH}/logger.h
    ${LIB_HEADERS_PATH}/loglevel.h
)

set(LIB_SOURCES
    configurator.cpp
    logger.cpp
)

add_library(${LIB_NAME}
    STATIC 
    ${LIB_HEADERS}
    ${LIB_SOURCES}
)

target_include_directories(${LIB_NAME}
    PUBLIC
    ../../include
   ${Boost_INCLUDE_DIR}
)

add_dependencies(${LIB_NAME} Boost)
