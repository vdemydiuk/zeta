//
//  session_description.h
//  zt
//
//  Created by Vyacheslav Demidyuk on 10/7/20.
//

#ifndef SDP_SESSION_DESCRIPTION_H
#define SDP_SESSION_DESCRIPTION_H

#include "description.h"
#include "media_description.h"
#include "origin.h"
#include "timing.h"

namespace zt
{
    namespace sdp
    {
        class SessionDescription : public Description
        {
        public:
            // ("v=")
            uint32_t getVersion() const;
            // ("s=")
            std::string getSessionName() const;
            // ("o=")
            std::shared_ptr<Origin> getOrigin() const;
            // ("i=")
            std::string getInformation() const;
            // ("u=")
            std::string getUri() const;
            // ("e=")
            std::string getEmail() const;
            // ("t=")
            std::shared_ptr<Timing> getTiming() const;
            
            uint32_t getMediaCount() const;
            std::shared_ptr<MediaDescription> getMedia(uint32_t index) const;
            
            void setVersion(uint32_t version);
            void setSessionName(const std::string& name);
            void setOrigin(const std::shared_ptr<Origin>& origin);
            void setInformation(const std::string& info);
            void setUri(const std::string& uri);
            void setEmail(const std::string& email);
            void setTiming(const std::shared_ptr<Timing>& timing);
            
            void addMediaDescription(const std::shared_ptr<MediaDescription>& media);

            std::string toString() const override;
        
        private:
            typedef std::vector<std::shared_ptr<MediaDescription>> MediaDescriptionVector;
            
            uint32_t                        version_{0};
            std::string                     session_name_{"-"};
            std::shared_ptr<Origin>         origin_;
            std::string                     information_;
            std::string                     uri_;
            std::string                     email_;
            std::shared_ptr<Timing>         timing_;

            MediaDescriptionVector          media_descriptions_;
        };
    };
}

#endif /* SDP_SESSION_DESCRIPTION_H */
