//
//  connection_data.h
//  zt
//
//  Created by Vyacheslav Demidyuk on 10/14/20.
//

#ifndef SDP_CONNECTION_DATA_H
#define SDP_CONNECTION_DATA_H

namespace zt
{
    namespace sdp
    {
        class ConnectionData
        {
        public:
            ConnectionData(const std::string& net_type, const std::string& addr_type, const std::string& addr, uint8_t ttl = 0)
            :net_type_(net_type)
            ,addr_type_(addr_type)
            ,addr_(addr)
            ,ttl_(ttl)
            {
            }
            
            std::string getNetType() const { return net_type_; }
            std::string getAddrType() const { return addr_type_; }
            std::string getAddr() const { return addr_; }
            uint8_t getTtl() const { return ttl_; }
            uint32_t getNumAddr() const { return num_addr_; }
            
            void setNetType(const std::string& net_type) { net_type_ = net_type; }
            void setAddrType(const std::string& addr_type) { addr_type_ = addr_type; }
            void setAddr(const std::string& addr) { addr_ = addr; }
            void setTtl(uint8_t ttl) { ttl_ = ttl; }
            void setNumAddr(uint32_t num) { num_addr_ = num; }
            
            std::string toString() const
            {
                std::string value;
                value += "c=" + net_type_ + " " + addr_type_ + " " + addr_;
                if (addr_type_ == "IP4" && ttl_ > 0)
                    value += "/" + std::to_string(ttl_);
                if (num_addr_ > 0)
                    value += "/" + std::to_string(num_addr_);
                value += "\r\n";
                return value;
            }
            
        private:
            std::string net_type_;
            std::string addr_type_;
            std::string addr_;
            uint8_t     ttl_;
            uint32_t    num_addr_{0};
        };
    }
}

#endif /* SDP_CONNECTION_DATA_H */
