//
//  parser.h
//  zt
//
//  Created by Vyacheslav Demidyuk on 11/11/20.
//

#ifndef SDP_PARSER_H
#define SDP_PARSER_H

#include <string>
#include <regex>
#include <map>
#include <vector>
#include <functional>
#include "session_description.h"

namespace zt
{
    namespace sdp
    {
        struct ParserRule
        {
            std::regex reg;
            std::function<void(const std::smatch&, Description*)> func_apply;
        };
        
        class SdpParser
        {
        public:
            static void Parse(const std::string& sdp, SessionDescription& session_description);
            
            static uint32_t toInt32(const std::string& str);
            static uint64_t toInt64(const std::string& str);
            static MediaType toMediaType(const std::string& str);
            static DirectionType toDirectionType(const std::string& str);
            static BandwidthType toBandwidthType(const std::string& str);
            
        private:
            SdpParser() {}
            
            static std::map<char,std::vector<ParserRule>> rules_;
            static std::map<std::string, MediaType> media_types_;
            static std::map<std::string, DirectionType> direction_types_;
            static std::map<std::string, BandwidthType> bandwidth_types_;
        };
    }
}

#endif /* SDP_PARSER_H */
